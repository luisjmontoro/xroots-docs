```image
src: https://dgpniqc313w3y.cloudfront.net/1.0.0-beta.92/img/public/Logo.svg
plain: true
```



## Version 1.0.5
#### ⚙️ **Componentes**:
    - Links:
        - ✅ Añadido - Primary light
        - ✅ Añadido - Secondary
        - ✅ Añadido - Estados active
        - ✅ Añadido - Documentación
    - Dropdowns:
        - ✅ Añadido - Multi-select con campo
        - ✅ Añadido - 💎 Librería de Sketch
        - ✅ Añadido - Documentación
    - Toggles:
        - ✅ Añadido - Switch y Switch + label
        - ✅ Añadido - 💎 Librería de Sketch
***

## Version 1.0.4
#### ⚙️ **Componentes**:
    - Links:
        - ✅ Añadido - Alineación izquierda.
    - Text inputs:
        - 🛠 Modificado - Varios fixes en textos e instancias.
        - ✅ Añadido - Excepción para Quote.
        - ✅ Añadido - Documentación.
    - Text Area:
        - ✅ Añadido - Basic y help text / Tamaño único / Ratio 16:9.
        - ✅ Añadido - 💎 Librería de Sketch.
    - Radio buttons:
        - ✅ Añadido - Documentación.
    - Dropdowns
        - ✅ Añadido - Foundations.
        - ✅ Añadido - Closed and open / xxxs - xxs.
        - ✅ Añadido - Simple y Multi-select.
    - Filter Tags
         - ✅ Añadido - Foundations para filter tags.



#### 🎨 **Primitives**:
    - Icons:
        - ✅ Añadido - Iconos de familias.
        - ✅ Añadido - Nuevos iconos en librería.
        - 🛠 Modificado - Documentación.

***

## Version 1.0.3
#### ⚙️ **Componentes**:
    - Buttons:
        - 🛠 Modificado - Estados y versiones.
        - ✅ Añadido - Tamaños s.
    - Text inputs:
        - 🛠 Modificado - Estados.
        - ✅ Añadido - Tamaños s.
    - Checkboxes:
        - ✅ Añadido - Tamaños xxxs, xxs.
        - ✅ Añadido - Documentación.
        - ✅ Añadido - 💎 Librería de Sketch.
    - Radio buttons:
        - ✅ Añadido - Tamaños xxxs, xxs.
        - ✅ Añadido - Documentación.
        - ✅ Añadido - 💎 Librería de Sketch.


#### 🎨 **Primitives**:
    - Icons:
        - ✅ Añadido - Iconos complejos.
        - ✅ Añadido - Repositorio SVG para front.
        - 🛠 Modificado - Documentación.
        - 🛠 Modificado - 💎 Librería de Sketch.
    - Spacing:
        - ❌ Eliminado - Valor del $spacing-4.


#### 📐 **Assets**:
    - Ilustraciones:
        - 🛠 Modificado - Documentación.
        - 🛠 Modificado - 💎 Librería de Sketch.







***

## Version 1.0.2

- ✍🏻 **Documentación:** 
    - ⚙️ **Componentes**:
        - Buttons
            - Uso y propiedades
            - Uso en librería de Sketch
            - Plugins
        - Text inputs
            - Uso y propiedades
            - Uso en librerías de Sketch
        - Component Status
    - ✍🏻 **Design Handbook - Abstract** 


- 💎 **Librerías de Sketch**:
    - ⚙️ **Componentes**:
        - Buttons (m, l, xl)
            - Main
            - Alternative
            - Navigation
            - Link
        - Text inputs (m, l)
            - Basic
            - Help text
    - 🎨 **Primitives**:
        - Iconos


- 📐 **Assets:**
    - Ilustraciones
    - Iconos

***

## Version 1.0.1

- **Documentación:** 
    - Icons
    - Layer
    - Spacing
    - Builder
    - Ilustraciones
    - Componentes:
        - Buttons
        - Text inputs
- **Librerías de Sketch**:
    - Layer
    - Spacing
    - Builder

- **Assets:**
    - Ilustraciones
    - Iconos

## Version 1.0

- Archivo raíz de **tokens** de color. Aplicación de color a tipografías.
- Archivo raíz de **tokens** de tipografía y su aplicación.
- Archivo raíz de **tokens** de breakpoints.
- **Librerías de Sketch**:
    - Color
    - Tipografía
    - Grid
- **Documentación**



### Próximas releases

- Generación de **librerías y tokens** para:
    - Icons
    - Ilustraciones
    - Buttons
    - Text inputs