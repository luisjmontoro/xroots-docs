## Definición

> **Los botones indican una acción / reacción / función concreta que tendrá lugar al hacer click sobre ellos. Generalmente los botones se utilizan para indicar acciones principales o secundarias, evitando su uso en items de navegación (salvo en el caso concreto y excepcional de tener una app móvil, en el que muy probablemente tendremos que contar con botones de navegación del estilo de Material Design).**

## Uso

```table
span: 5
columns:
  - Tipo
  - Descripción y uso
rows:
  - Tipo: '**Primary**'
    Descripción y uso: Botón utilizado para acciones principales.
  - Tipo: '**Alternative**'
    Descripción y uso: Botón utilizado para acciones secundarias y, ocasionalmente, para algunas acciones principales.
  - Tipo: '**Icon button**'
    Descripción y uso: Botón que puede ser del tipo **primary** o **alternative** al que se le añade un icono. El icono puede estar a la izquierda o a la derecha del CTA. 
```

```image
span: 5
src: "https://gitlab.com/luisjmontoro/xroots-docs/raw/8537e7445f2aca6cecdc5031bde91acc7d875037/catalog/static/img/ccomponent-buttons-types.png"
plain: true
```


## Propiedades
***

### Tamaños y spacing

Todos los botones tienen un alto definido según su tamaño. En cuanto al ancho, todos tienen un ancho mínimo (min-width) definido según su tamaño. Si el CTA superase el ancho mínimo, entonces se aplica un padding lateral de **$spacing-6 (24px)** por cada lado.

En los **icon buttons**, entre el icono y el CTA hay un **$spacing-3 (8px)**. 

```image
span: 5
src: "https://gitlab.com/luisjmontoro/xroots-docs/raw/313611e271166fb5cf5737a013aed4fce1eb0a89/catalog/static/img/component-button-spacing.png"
plain: true
```

Las medidas según su tamaño son:

```table
span: 5
columns:
  - size
  - height
  - min-width
  - font-size
rows:
  - size: '**sm**'
    height: 40px
    min-width: 148px
    font-size: '{size.font.desktop.m.value}'
  - size: '**md**'
    height: 48px
    min-width: 148px
    font-size: '{size.font.desktop.m.value}'
  - size: '**lg**'
    height: 56px
    min-width: 160px
    font-size: '{size.font.desktop.m.value}'
  - size: '**xl**'
    height: 64px
    min-width: 160px
    font-size: '{size.font.desktop.m.value}'
```

```image
    span: 5
    src: "https://gitlab.com/luisjmontoro/xroots-docs/raw/ee8ab30445a618f2f47040d276cca91db5c6bcb7/catalog/static/img/ccomponent-buttons-sizes.png"
    plain: true
    description: "_Ejemplo: button / primary / simple / default_"
```

```hint
⚠️ El ancho de los botones crece cuando el literal del CTA, incluyendo su padding ($spacing-6), es superior al min-width.
```
#### Especial atención (plugin required): 
```hint
⚠️ Cuando necesitemos sobrescribir el literal del botón con un texto que vaya a superar el ancho mínimo, utilizaremos el plugin [Magic Buttons](https://github.com/upxlabs/magic-buttons-sketch-plugin/raw/master/magic-buttons-sketch-plugin-v01.zip) (también se puede xinstalar desde Runner). Una vez instalado el plugin, utilizaremos el atajo de teclado: cmd + shift + m.
```


### Color

```table
span: 6
columns:
  - Tipo
  - Fill
  - Border
  - Font
rows:
  - Tipo: '**Dark**'
    Fill: '{color.root.brand.primary.base.value}'
    Border: none
    Font: '{color.font.white.value}'
  - Tipo: '**Light**'
    Fill: '{color.root.ui.opacity.transparent.value}'
    Border: '{color.root.brand.primary.base.value} 2px width'
    Font: '{color.root.brand.primary.base.value}'
```

### Estados

Los estados del botón son cuatro:

- **Default:** estado por defecto del botón activado.
- **Hover:** estado del botón cuando el cursor se sitúa por encima del mismo.
- **Pressed:** estado del botón durante el momento en el que es accionado.
- **Disabled:** estado del botón desactivado, visible pero no disponible para el usuario.

```hint
⚠️ El estado *disabled* ha de disparar por defecto el cursor *not-allowed;* cuando el usuario haga hover sobre el mismo.
```

Los estados se construyen, en Sketch, a través de capas de overlay dispuestas por el **builder**.

##### **Primary Dark**

```table
span: 6
columns:
  - Estado
  - Background color
  - Font color
rows:
  - Estado: '**Default**'
    Background color: '{color.root.brand.primary.base.value}'
    Font color: '{color.root.ui.neutral.white.value}'
  - Estado: '**Hover**'
    Background color: '{color.root.brand.primary.base75.value}'
    Font color: '{color.root.ui.neutral.white.value}'
  - Estado: '**Pressed**'
    Background color: 'shade($primary-base75, 25%)'
    Font color: '{color.root.ui.neutral.white.value}'
  - Estado: '**Disabled**'
    Background color: 'tint($primary-base50, 65%)'
    Font color: '{color.root.ui.neutral.white.value}'
```

```image
    span: 5
    src: "https://gitlab.com/luisjmontoro/xroots-docs/raw/8c2124926ba366ff3ab050c0bdb9f5b65854a7a4/catalog/static/img/ccomponent-buttons-states-primary-dark.png"
    plain: true
    description: "_Ejemplo: button / simple / lg_"
```
##### **Primary Light**

```table
span: 6
columns:
  - Estado
  - Background color
  - Font color
rows:
  - Estado: '**Default**'
    Background color: '{color.root.ui.neutral.white.value}'
    Font color: '{color.root.brand.primary.base.value}'
  - Estado: '**Hover**'
    Background color: '{color.root.brand.primary.base75.value}'
    Font color: '{color.root.ui.neutral.white.value}'
  - Estado: '**Pressed**'
    Background color: 'shade($primary-base25, 25%)'
    Font color: '{color.root.ui.neutral.white.value}'
  - Estado: '**Disabled**'
    Background color: '{color.root.ui.neutral.white.value}'
    Font color: 'tint($primary-base, 65%)'
```

```image
    span: 5
    src: "https://gitlab.com/luisjmontoro/xroots-docs/raw/8c2124926ba366ff3ab050c0bdb9f5b65854a7a4/catalog/static/img/ccomponent-buttons-states-primary-light.png"
    plain: true
    description: "_Ejemplo: button / simple / lg_"
```
##### **Secondary Dark**

```table
span: 6
columns:
  - Estado
  - Background color
  - Border color
  - Font color
rows:
  - Estado: '**Default**'
    Background color: transparent
    Border color: '{color.root.brand.primary.base.value}'
    Font color: '{color.root.brand.primary.base.value}'
  - Estado: '**Hover**'
    Background color: '{color.root.brand.primary.base.value}'
    Font color: '{color.root.ui.neutral.white.value}'
  - Estado: '**Pressed**'
    Background color: 'shade($primary-base, 25%)'
    Font color: '{color.root.ui.neutral.white.value}'
  - Estado: '**Disabled**'
    Background color: 'tint($primary-base, 65%)'
    Font color: 'tint($primary-base, 65%)'
```

```image
    span: 5
    src: "https://gitlab.com/luisjmontoro/xroots-docs/raw/8c2124926ba366ff3ab050c0bdb9f5b65854a7a4/catalog/static/img/ccomponent-buttons-states-secondary-dark.png"
    plain: true
    description: "_Ejemplo: button / simple / lg_"
```
##### **Secondary Light**

```table
span: 6
columns:
  - Estado
  - Background color
  - Border color
  - Font color
rows:
  - Estado: '**Default**'
    Background color: transparent
    Border color: '{color.root.ui.neutral.white.value}'
    Font color: '{color.root.ui.neutral.white.value}'
  - Estado: '**Hover**'
    Background color: '{color.root.ui.neutral.white.value}'
    Font color: '{color.root.brand.primary.base.value}'
  - Estado: '**Pressed**'
    Background color: 'shade($white, 25%)'
    Font color: '{color.root.brand.primary.base.value}'
  - Estado: '**Disabled**'
```

```image
    span: 5
    src: "https://gitlab.com/luisjmontoro/xroots-docs/raw/8c2124926ba366ff3ab050c0bdb9f5b65854a7a4/catalog/static/img/ccomponent-buttons-states-secondary-light.png"
    plain: true
    description: "_Ejemplo: button / simple / lg_"
```

```hint
⚠️ El botón **secondary light** no se utilizará nunca en el estado *disabled*.
```



## Construcción de los botones en Sketch
***

El componente botón se construye en Sketch utilizando los elementos del **builder** en un orden determinado y por una razón concreta. El orden de las capas, de abajo a arriba, es el siguiente:

- **Layer:** Determina el grado de elevación que tiene el botón sobre la base según su estado. En Sketch proyecta una sombra que será visible o no dependiendo del estado del botón.
- **Fill:** Determina las propiedades del relleno del botón, que serán diferentes dependiendo de si se trata del botón **primary** o **alternative**, por ejemplo.
- **Border:** Determina las propiedades del borde del botón. Ha de ir sobre el fill para garantizar su visibilidad. Entre sus propiedades se encuentra el grosor (border-width), el color (border-color) y sus vértices (border-radius).
- **Text:** El CTA del botón se añade mediante los estilos de texto de la librería primitiva de tipografías, utilizando un estilo tipográfico u otro según marque la guía de estilos de la marca.
- **State:** Por último, y también proviniente del **builder** se añade una capa de State, que determina el estado del componente entre **default**, **hover**, **pressed** o **disabled**. Por defecto, la capa del estado default será invisible en Sketch, pero ha de estar. **IMPORTANTE**: En el estado **hover**, la capa de **state** irá justo por debajo de la capa **text**, para que de este modo el texto no se empañe con el overlay.

## Overrides en Sketch

Por defecto, en los iconos simples solo podemos sobrescribir el **valor** del texto, no su estilo. Para conseguirlo en las instancias, cada símbolo requiere una **configuración de overrides** concreta.

En los botones con icono, además se puede sobrescribir el **símbolo del icono y el estilo** del mismo. De este modo podemos utilizar el icono que mejor se adecúa al CTA.

#### Especial atención (plugin required): 
```hint
⚠️ Cuando necesitemos sobrescribir el literal del botón con un texto que vaya a superar el ancho mínimo, utilizaremos el plugin [Magic Buttons](https://github.com/upxlabs/magic-buttons-sketch-plugin/raw/master/magic-buttons-sketch-plugin-v01.zip) (también se puede xinstalar desde Runner). Una vez instalado el plugin, utilizaremos el atajo de teclado: cmd + shift + m.
```