## Definición

> **Los radiobuttons se utilizan principalmente en listas de elementos en las que el usuario puede seleccionar únicamente una opción.**

## **1. Uso**

```table
span: 5
columns:
  - Tipo
  - Descripción y uso
rows:
  - Tipo: '**Input + Label**'
    Descripción y uso: El label debe acompañar siempre al input en el caso de los radio buttons.
```

```image
span: 5
src: "https://gitlab.com/luisjmontoro/xroots-docs/raw/5a2b070e30228bcc07fc559341ebc18d12b80429/catalog/static/img/component-radiobutton-default.png"
plain: true
description: "Ejemplo: default and disabled / xxxs"
```


## **2. Propiedades**
***

### **2.1. Tamaños y spacing**

Todos los radio-buttons son circulares y mantienen el ratio de la proporción independientemente de su tamaño.

El espacio entre el input y el label es de **$spacing-3 (8px)**. 

```image
span: 5
src: "https://gitlab.com/luisjmontoro/xroots-docs/raw/5a2b070e30228bcc07fc559341ebc18d12b80429/catalog/static/img/component-radiobiutton-spacing.png"
plain: true
description: "Ejemplo: default / xxs / spacing-3"
```

Las medidas según su tamaño son:

```table
span: 5
columns:
  - size
  - px
  - font-size
rows:
  - size: '**xxxs**'
    px: 16x16px
    font-size: '{size.font.desktop.s.value}'
  - size: '**xxs**'
    px: 24x24px
    font-size: '{size.font.desktop.m.value}'
  - size: '**xs**'
    px: 32x32px
    font-size: '{size.font.desktop.m.value}'
```



### **2.2. Color**

```table
span: 6
columns:
  - Tipo
  - Fill
  - Border
  - Icon
rows:
  - Tipo: '**Dark**'
    Fill: '{color.root.ui.neutral.white.value}'
    Border: '{color.root.ui.neutral.base60.value} 1px width'
    Icon: '{color.root.ui.neutral.white.base.value}'
```

### **2.3. Estados**

Los estados del radio button son cuatro:

- **Unchecked:** estado del radio button que no está seleccionado.
- **Checked:** estado del radio button seleccionado por el usuario.
- **Disabled:** estado del radio button desactivado, visible pero no disponible para el usuario.

```hint
⚠️ El estado *disabled* ha de disparar por defecto el cursor *not-allowed;* cuando el usuario haga hover sobre el mismo.
```

Los estados se construyen, en Sketch, a través de capas de overlay dispuestas por el **builder**.

##### **Dark**

```table
span: 6
columns:
  - Estado
  - Border color
  - Font color
rows:
  - Estado: '**Default**'
    Border color: '{color.root.ui.neutral.base60.value}'
    Font color: '{color.root.ui.indigo.base.value}'
  - Estado: '**Disabled**'
    Border color: 'tint($neutral-base60, 65%)'
    Font color: '{color.root.ui.indigo.base50.value}'
```

```image
    span: 5
    src: "https://gitlab.com/luisjmontoro/xroots-docs/raw/5a2b070e30228bcc07fc559341ebc18d12b80429/catalog/static/img/component-radiobutton-status-dark.png"
    plain: true
    description: "Ejemplo: radio button / dark / left / default"
```



## **3. Construcción de los radio buttons en Sketch**
***

El componente radio button se construye en Sketch utilizando los elementos del **builder** en un orden determinado y por una razón concreta. El orden de las capas, de abajo a arriba, es el siguiente:

- **Fill:** Determina las propiedades del relleno del input, que serán diferentes dependiendo del estado (**unchecked, checked, intermediate**).
- **Border:** Determina las propiedades del borde del input. Ha de ir sobre el fill para garantizar su visibilidad. Entre sus propiedades se encuentra el grosor (border-width), el color (border-color) y sus vértices (border-radius).
- **Label:** El label del radio button se añade mediante los estilos de texto de la librería primitiva de tipografías, utilizando un estilo tipográfico u otro según marque la guía de estilos de la marca.
- **State:** Por último, y también proviniente del **builder** se añade una capa de State, que determina el estado del componente entre **default** o **disabled**. Por defecto, la capa del estado default será invisible en Sketch, pero ha de estar. **IMPORTANTE**: La capa del estado (state) por irá solo sobre el **input** del checkbox ocupando toda su área.


