## Definición

> **Los links son elementos básicos de navegación. También disparan acciones secundarias o terciarias (*Cambiar contraseña*), o indican al usuario la posibilidad de realizar interacciones concretas (*Ver detalles*).**

## **1. Uso**

```table
span: 5
columns:
  - Tipo
  - Descripción y uso
rows:
  - Tipo: '**Primary**'
    Descripción y uso: Link primario, el utilizado por defecto.
  - Tipo: '**Secondary**'
    Descripción y uso: Enlace secundario.
```

```image
span: 5
src: "https://gitlab.com/luisjmontoro/xroots-docs/raw/a5136c5f3ddbb7ec390957a72b48e134414d106f/catalog/static/img/componente-link-md-default.png"
plain: true
description: "_Ejemplo: default / md_"
```


## **2. Propiedades**
***

### **2.1. Sizing**

Los estilos tipográficos de los enlaces cambian en función del tamaño elegido. Es necesario tener distintos tamaños para las distintas aplicaciones de los enlaces.

Las medidas según su tamaño son:

```table
span: 5
columns:
  - size
  - font-weight
rows:
  - size: '**xs**'
    font-weight: '{font.weight.1.value} / 300'
  - size: '**sm**'
    font-weight: '{font.weight.2.value} / 400'
  - size: '**md**'
    font-weight: '{font.weight.3.value} / 700'
```

### **2.2. Color**

El color del enlace viene determinado por el tipo de enlace que es:

```table
span: 5
columns:
  - Tipo
  - Color
rows:
  - Tipo: '**Primary**'
    Color: '{color.root.brand.primary.base.value}'
  - Tipo: '**Secondary**'
    Color: '{color.root.ui.text.base.value}'
```

```hint
⚠️ Este color está sujeto a modificaciones sobre su tono según el estado del componente.
```

### **2.3. Estados**

#### **2.3.1. Primary**

```table
span: 6
columns:
  - Estado
  - Color
rows:
  - Estado: '**Default**'
    Color: '{color.root.brand.primary.base.value}'
  - Estado: '**Hover**'
    Color: '{color.root.brand.primary.base50.value}'
  - Estado: '**Pressed**'
    Color: '{color.root.brand.primary.base50.value} / underline'
  - Estado: '**Disabled**'
    Color: 'tint($primary-base50, 65%)'
  - Estado: '**Active**'
    Color: '{color.root.brand.primary.base50.value}'
```

```image
span: 5
src: "https://gitlab.com/luisjmontoro/xroots-docs/raw/61319eb105736c823e770d6b9efb0d130353918a/catalog/static/img/component-link-states.png"
plain: true
description: "_Ejemplo: default / md_"
```

#### **2.3.2. Primary light**

```table
span: 6
columns:
  - Estado
  - Color
rows:
  - Estado: '**Default**'
    Color: '{color.root.ui.neutral.white.value}'
  - Estado: '**Hover**'
    Color: '{color.root.ui.neutral.white.value} / underline'
  - Estado: '**Pressed**'
    Color: '{color.root.ui.neutral.white.value} / underline'
  - Estado: '**Disabled**'
    Color: 'tint($white, 65%)'
```


#### **2.3.2. Secondary**

```table
span: 6
columns:
  - Estado
  - Color
rows:
  - Estado: '**Default**'
    Color: '{color.root.ui.text.base.value}'
  - Estado: '**Hover**'
    Color: '{color.root.ui.text.base75.value}'
  - Estado: '**Pressed**'
    Color: '{color.root.ui.text.base75.value} / underline'
  - Estado: '**Disabled**'
    Color: 'tint($neutral-base50, 65%)'
  - Estado: '**Active**'
    Color: '{color.root.ui.text.base75.value}'
```

```image
span: 5
src: "https://gitlab.com/luisjmontoro/xroots-docs/raw/61319eb105736c823e770d6b9efb0d130353918a/catalog/static/img/component-link-secondary-states.png"
plain: true
description: "_Ejemplo: default / md_"
```

