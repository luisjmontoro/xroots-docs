## Definición

> **Los checkboxes se utilizan principalmente en listas de elementos en las que el usuario puede seleccionar una opción, todas o ninguna.**

## **1. Uso**

```table
span: 5
columns:
  - Tipo
  - Descripción y uso
rows:
  - Tipo: '**Switch**'
    Descripción y uso: 'Input con forma de interruptor con dos posiciones: on / off.'
  - Tipo: '**Switch + Label**'
    Descripción y uso: Input acompañado de una etiqueta que indica la acción que realiza.
```

```image
span: 5
src: "https://gitlab.com/luisjmontoro/xroots-docs/raw/65e7c196afe272a5ba6d52003d20b37538863bb5/catalog/static/img/component-checkbox-default.png"
plain: true
description: "Ejemplo: default and disabled / xxxs"
```


## **2. Propiedades**
***

### **2.1. Tamaños y spacing**

Los switch de los toggles son elementos rectangulares cuyos bordes tienen un border-radius del 100%. Definimos un **ancho y alto** específico para cada uno de ellos.

En el caso de los toggles compuestos de **switch + label** el espacio entre el input y el label es de **$spacing-3 (8px)**. 

```image
span: 5
src: "https://gitlab.com/luisjmontoro/xroots-docs/raw/2feff4a3674adc5f03ce0c55b79bf1ac05d2b5d8/catalog/static/img/component-checkbox-spacing.png"
plain: true
description: "Ejemplo: default / xxs / spacing-3"
```

Las medidas según su tamaño son:

```table
span: 5
columns:
  - size
  - px
  - font-size
rows:
  - size: '**xxxs**'
    px: 25x16px
    font-size: '{size.font.desktop.s.value}'
  - size: '**xxs**'
    px: 40x24px
    font-size: '{size.font.desktop.m.value}'
  - size: '**xs**'
    px: 53x32px
    font-size: '{size.font.desktop.m.value}'
```



### **2.2. Color**

```table
span: 6
columns:
  - Tipo
  - Fill
  - Border
rows:
  - Tipo: '**Default / Off**'
    Fill: '{color.root.ui.neutral.base30.value}'
    Border: '{color.root.ui.opacity.transparent.value}'
  - Tipo: '**Default / On**'
    Fill: '{color.root.ui.support.success.base75.value}'
    Border: '{color.root.ui.opacity.transparent.value}'
```

### **2.3. Estados**

Los estados del toggle son dos:

- **Off:** estado por defecto del toggle desactivado.
- **On:** estado por defecto del toggle activado.
- **Disabled:** estado por defecto del toggle cuando no es accionable.

```hint
⚠️ El estado *disabled* ha de disparar por defecto el cursor *not-allowed;* cuando el usuario haga hover sobre el mismo.
```

```image
    span: 5
    src: "https://gitlab.com/luisjmontoro/xroots-docs/raw/abcdb22f6d84ad2bdcb18d147bdf507ca1e529b1/catalog/static/img/component-checkbox-status-dark.png"
    plain: true
    description: "Ejemplo: checkbox / light / left / default"
```

El estado **disabled** se construye, en Sketch, a través de una capa de overlay dispuesta por el **builder**.

##### **Dark**

```table
span: 6
columns:
  - Estado
  - Border color
  - Font color
rows:
  - Estado: '**Default**'
    Border color: '{color.root.ui.neutral.base60.value}'
    Font color: '{color.root.ui.indigo.base.value}'
  - Estado: '**Disabled**'
    Border color: 'tint($neutral-base60, 65%)'
    Font color: '{color.root.ui.indigo.base50.value}'
```

```image
    span: 5
    src: "https://gitlab.com/luisjmontoro/xroots-docs/raw/abcdb22f6d84ad2bdcb18d147bdf507ca1e529b1/catalog/static/img/component-checkbox-status-light.png"
    plain: true
    description: "Ejemplo: checkbox / dark / left / default"
```
##### **Light**

```table
span: 6
columns:
  - Estado
  - Border color
  - Background color
  - Font color
rows:
  - Estado: '**Default**'
    Border color: '{color.root.ui.neutral.white.value}'
    Font color: '{color.root.ui.neutral.white.value}'
```

```image
    span: 5
    src: "https://gitlab.com/luisjmontoro/xroots-docs/raw/c6fffd49c0fd0cabd2b70c2e5d101542fb4be8b9/catalog/static/img/component-checkbox-light-default.png"
    plain: true
    description: "Ejemplo: checkbox / light / right / default"
```




