## Definición

> **Los text inputs son los elementos de la interfaz que permiten a los usuarios interactuar introduciendo datos. Normalmente forman parte de los formularios, aunque también podemos encontrarlos por separado (por ejemplo: la caja de búsqueda). Siempre van acompañados de un input que ofrece al usuario la posibilidad de completar la acción.**

## Uso

```table
span: 5
columns:
  - Tipo
  - Descripción y uso
rows:
  - Tipo: '**Text Input**'
    Descripción y uso: Campo de texto para que el usuario introduzca una palabra o una frase corta.
  - Tipo: '**Text Area**'
    Descripción y uso: Campo de texto para que el usuario introduzca un párrafo.
``` 

## Composición

El componente text input cuenta en su composición con elementos diferentes según la casuística. Los **elementos en negrita** son los mínimos necesarios para considerar el componente text input como tal:

- **Text label**
- Help text
- **Input (w/ placeholder)**
- Icon
- Support message (solo visible en los diferentes estados de validación.


```image
    span: 5
    src: "https://gitlab.com/luisjmontoro/xroots-docs/raw/7a70f970144a2e2dd805a48bf7b8956d5196bbb6/catalog/static/img/component-text-input-example.png"
    plain: true
    description: "_Ejemplo: text input / md_"
```

## Propiedades
***

### Sizing

```table
span: 5
columns:
  - size
  - height
  - label font-size
  - placeholder font-size
rows:
  - size: '**sm**'
    height: 40px
    label font-size: '{size.font.base.value}'
    placeholder font-size: '{size.font.base.value}'
  - size: '**md**'
    height: 48px
    label font-size: '{size.font.base.value}'
    placeholder font-size: '{size.font.base.value}'
  - size: '**lg**'
    height: 56px
    label font-size: '{size.font.base.value}'
    placeholder font-size: '{size.font.base.value}'
```

### Spacing

```table
span: 6
columns:
  - size
  - element
  - padding
  - margin
  - spacing
rows:
  - size: '**all**'
    element: label
    padding: '0'
    margin: 0 0 8px 0
    spacing: '{size.spacing.3.value}'
  - size: '**all**'
    element: help text
    padding: '0'
    margin: 0 0 16px 0
    spacing: '{size.spacing.4.value}'
  - size: '**all**'
    element: input (placeholder)
    padding: 0 16px
    margin: 0
    spacing: '{size.spacing.4.value}'
  - size: '**all**'
    element: icon
    padding: 0
    margin: 0 8px (left or right)
    spacing: '{size.spacing.3.value}'
  - size: '**all**'
    element: support message
    padding: 0
    margin: 8px 0 0 0
    spacing: '{size.spacing.3.value}'
  - size: '**all**'
    element: component
    padding: '0'
    margin: 0 0 24px 0
    spacing: '{size.spacing.5.value}'
```
```image
    span: 5
    src: "https://gitlab.com/luisjmontoro/xroots-docs/raw/be5909511416501358cc636b7cbc088127b4eca7/catalog/static/img/component-text-input-spacing.png"
    plain: true
    description: "_Ejemplo: text input / help text / md / error_"
```

### Color

```table
span: 6
columns:
  - Estado
  - Fill
  - Border
  - Font / icon
rows:
  - Estado: '**Default**'
    Fill: '{color.root.ui.neutral.white.value}'
    Border: '{color.root.ui.neutral.base60.value}'
    Font / icon: '{color.font.indigo-base50.value}'
  - Estado: '**Active**'
    Fill: '{color.root.ui.neutral.white.value}'
    Border: '{color.root.ui.neutral.base60.value}'
    Font / icon: '{color.font.indigo-base.value}'
  - Estado: '**Error**'
    Fill: '{color.root.ui.support.white.value}'
    Border: '{color.root.ui.support.error.base.value}'
    Font / icon: '{color.font.indigo-base50.value}'
  - Estado: '**Success**'
    Fill: '{color.root.ui.support.white.value}'
    Border: '{color.root.ui.support.success.base.value}'
    Font / icon: '{color.font.indigo-base50.value}'
  - Estado: '**Disabled**'
    Fill: 'ligthen($default-value, 65%)'
    Border: 'ligthen($default-value, 65%)'
    Font / icon: 'ligthen($default-value, 65%)'

```
```image
    span: 8
    src: "https://gitlab.com/luisjmontoro/xroots-docs/raw/50fd70af8d901bf4da9519727aa4789bf42c0955/catalog/static/img/component-text-input-color.png"
    plain: true
    description: "_Ejemplo: text input / basic / md_"
```