## Definición

> **Los checkboxes se utilizan principalmente en listas de elementos en las que el usuario puede seleccionar una opción, todas o ninguna.**

## **1. Uso**

```table
span: 5
columns:
  - Tipo
  - Descripción y uso
rows:
  - Tipo: '**Input**'
    Descripción y uso: Es la caja accionable por el usuario. Puede funcionar de manera independiente en algunos casos.
  - Tipo: '**Input + Label**'
    Descripción y uso: El input acompañado de label se utiliza principalmente en listas de elementos.
```

```image
span: 5
src: "https://gitlab.com/luisjmontoro/xroots-docs/raw/65e7c196afe272a5ba6d52003d20b37538863bb5/catalog/static/img/component-checkbox-default.png"
plain: true
description: "Ejemplo: default and disabled / xxxs"
```


## **2. Propiedades**
***

### **2.1. Tamaños y spacing**

Todos los checkboxes son cuadrados y se mantiene el ratio de la proporción independientemente de su tamaño.

En el caso de los checkboxes compuestos de **input + label** el espacio entre el input y el label es de **$spacing-3 (8px)**. 

```image
span: 5
src: "https://gitlab.com/luisjmontoro/xroots-docs/raw/2feff4a3674adc5f03ce0c55b79bf1ac05d2b5d8/catalog/static/img/component-checkbox-spacing.png"
plain: true
description: "Ejemplo: default / xxs / spacing-3"
```

Las medidas según su tamaño son:

```table
span: 5
columns:
  - size
  - px
  - font-size
rows:
  - size: '**xxxs**'
    px: 16x16px
    font-size: '{size.font.desktop.s.value}'
  - size: '**xxs**'
    px: 24x24px
    font-size: '{size.font.desktop.m.value}'
  - size: '**xs**'
    px: 32x32px
    font-size: '{size.font.desktop.m.value}'
```



### **2.2. Color**

```table
span: 6
columns:
  - Tipo
  - Fill
  - Border
  - Icon
rows:
  - Tipo: '**Dark**'
    Fill: '{color.root.ui.neutral.white.value}'
    Border: '{color.root.ui.neutral.base60.value} 1px width'
    Icon: '{color.root.ui.neutral.white.base.value}'
  - Tipo: '**Light**'
    Fill: '{color.root.ui.opacity.transparent.value}'
    Border: '{color.root.ui.neutral.white.base.value} 1px width'
    Icon: '{color.root.ui.neutral.white.base.value}'
```

### **2.3. Estados**

Los estados del checkbox son cuatro:

- **Unchecked:** estado por defecto del checkbox activado.
- **Checked:** estado del checkbox cuando el usuario lo ha marcado.
- **Indeterminate:** estado del checkbox que agrupa un listado de opciones de las cuales no todas estám seleccionadas.
- **Disabled:** estado del checkbox desactivado, visible pero no disponible para el usuario.

```hint
⚠️ El estado *disabled* ha de disparar por defecto el cursor *not-allowed;* cuando el usuario haga hover sobre el mismo.
```

```image
    span: 5
    src: "https://gitlab.com/luisjmontoro/xroots-docs/raw/abcdb22f6d84ad2bdcb18d147bdf507ca1e529b1/catalog/static/img/component-checkbox-status-dark.png"
    plain: true
    description: "Ejemplo: checkbox / light / left / default"
```

Los estados se construyen, en Sketch, a través de capas de overlay dispuestas por el **builder**.

##### **Dark**

```table
span: 6
columns:
  - Estado
  - Border color
  - Font color
rows:
  - Estado: '**Default**'
    Border color: '{color.root.ui.neutral.base60.value}'
    Font color: '{color.root.ui.indigo.base.value}'
  - Estado: '**Disabled**'
    Border color: 'tint($neutral-base60, 65%)'
    Font color: '{color.root.ui.indigo.base50.value}'
```

```image
    span: 5
    src: "https://gitlab.com/luisjmontoro/xroots-docs/raw/abcdb22f6d84ad2bdcb18d147bdf507ca1e529b1/catalog/static/img/component-checkbox-status-light.png"
    plain: true
    description: "Ejemplo: checkbox / dark / left / default"
```
##### **Light**

```table
span: 6
columns:
  - Estado
  - Border color
  - Background color
  - Font color
rows:
  - Estado: '**Default**'
    Border color: '{color.root.ui.neutral.white.value}'
    Font color: '{color.root.ui.neutral.white.value}'
```

```image
    span: 5
    src: "https://gitlab.com/luisjmontoro/xroots-docs/raw/c6fffd49c0fd0cabd2b70c2e5d101542fb4be8b9/catalog/static/img/component-checkbox-light-default.png"
    plain: true
    description: "Ejemplo: checkbox / light / right / default"
```



## **3. Construcción del checkbox en Sketch**
***

El componente checkbox se construye en Sketch utilizando los elementos del **builder** en un orden determinado y por una razón concreta. El orden de las capas, de abajo a arriba, es el siguiente:

- **Fill:** Determina las propiedades del relleno del input, que serán diferentes dependiendo del estado (**unchecked, checked, indeterminate**) o la versión (**dark, light**).
- **Border:** Determina las propiedades del borde del input. Ha de ir sobre el fill para garantizar su visibilidad. Entre sus propiedades se encuentra el grosor (border-width), el color (border-color) y sus vértices (border-radius).
- **Label:** El label del checkbox se añade mediante los estilos de texto de la librería primitiva de tipografías, utilizando un estilo tipográfico u otro según marque la guía de estilos de la marca.
- **State:** Por último, y también proviniente del **builder** se añade una capa de State, que determina el estado del componente entre **default** o **disabled**. Por defecto, la capa del estado default será invisible en Sketch, pero ha de estar. **IMPORTANTE**: La capa del estado (state) por irá solo sobre el **input** del checkbox ocupando toda su área.


