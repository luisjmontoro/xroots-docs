## Definición

> **Los dropdowns permiten al usuario desplegar una serie de opciones de una lista concreta (inicialmente oculta), de la cual pueden elegir uno o más elementos. Para garantizar una buena experiencia de usuario, nunca dejamos visibles más de 9 elementos cuando el componente dropdown está desplegado (abierto).**

## **1. Uso**

```table
span: 5
columns:
  - Tipo
  - Descripción y uso
rows:
  - Tipo: '**Simple**'
    Descripción y uso: El dropdown simple deja elegir una sola opción del listado, siendo ésta excluyente respecto a las demás.
  - Tipo: '**Multi-select**'
    Descripción y uso: El dropdown multi-select permite elegir más de una opción de la lista, estando cada una de éstas acompañada de un checkbox.
``` 

## **2. Composición**

El componente dropdown cuenta en su composición con elementos diferentes según la casuística. Los **elementos en negrita** son los mínimos necesarios para considerar el componente dropdown como tal:

- **Text label**
- Help text
- **Input (w/ placeholder)**
- **Icon (arrow)**
- Support message (solo visible en los diferentes estados de validación.


```image
    span: 5
    src: "https://gitlab.com/luisjmontoro/xroots-docs/raw/761debeaf34d5e826702c079237a77c0035228de/catalog/static/img/component-dropdown-closed-default.png"
    plain: true
    description: "_Ejemplo: dropdown / md / closed_"
```
```image
    span: 5
    src: "https://gitlab.com/luisjmontoro/xroots-docs/raw/761debeaf34d5e826702c079237a77c0035228de/catalog/static/img/component-dropdown-open.png"
    plain: true
    description: "_Ejemplo: dropdown / md / open_"
```

## **3. Propiedades**
***

### **3.1. Sizing**

Las propiedades de los **tamaños** son las mismas tanto para el dropdown cerrado como para cada item del dropdown desplegado.

```table
span: 5
columns:
  - size
  - height
  - label font-size
  - placeholder font-size
rows:
  - size: '**sm**'
    height: 40px
    label font-size: '{size.font.base.value}'
    placeholder font-size: '{size.font.base.value}'
  - size: '**md**'
    height: 48px
    label font-size: '{size.font.base.value}'
    placeholder font-size: '{size.font.base.value}'
  - size: '**lg**'
    height: 56px
    label font-size: '{size.font.base.value}'
    placeholder font-size: '{size.font.base.value}'
```

### **3.2. Spacing**

Las propiedades de **spacing** son las mismas tanto para el dropdown cerrado como para cada item del dropdown desplegado.


```table
span: 6
columns:
  - size
  - element
  - padding
  - margin
  - spacing
rows:
  - size: '**all**'
    element: label
    padding: '0'
    margin: 0 0 8px 0
    spacing: '{size.spacing.3.value}'
  - size: '**all**'
    element: help text
    padding: '0'
    margin: 0 0 16px 0
    spacing: '{size.spacing.4.value}'
  - size: '**all**'
    element: input (placeholder)
    padding: 0 16px
    margin: 0
    spacing: '{size.spacing.4.value}'
  - size: '**all**'
    element: icon (to placeholder)
    padding: 0
    margin: 0 8px (left or right)
    spacing: '{size.spacing.3.value}'
  - size: '**all**'
    element: support message
    padding: 0
    margin: 8px 0 0 0
    spacing: '{size.spacing.3.value}'
  - size: '**all**'
    element: component
    padding: '0'
    margin: 0 0 24px 0
    spacing: '{size.spacing.5.value}'
```
```image
    span: 5
    src: "https://gitlab.com/luisjmontoro/xroots-docs/raw/761debeaf34d5e826702c079237a77c0035228de/catalog/static/img/component-dropdown-spacing.png"
    plain: true
    description: "_Ejemplo: dropdown / simple / closed / default_"
```

### **3.3. Color**

```table
span: 6
columns:
  - Estado
  - Fill
  - Border
  - Font / icon
rows:
  - Estado: '**Default**'
    Fill: '{color.root.ui.neutral.white.value}'
    Border: '{color.root.ui.neutral.base60.value}'
    Font / icon: '{color.font.indigo-base50.value}'
  - Estado: '**Active**'
    Fill: '{color.root.ui.neutral.white.value}'
    Border: '{color.root.ui.neutral.base60.value}'
    Font / icon: '{color.font.indigo-base.value}'
  - Estado: '**Error**'
    Fill: '{color.root.ui.support.white.value}'
    Border: '{color.root.ui.support.error.base.value}'
    Font / icon: '{color.font.indigo-base50.value}'
  - Estado: '**Success**'
    Fill: '{color.root.ui.support.white.value}'
    Border: '{color.root.ui.support.success.base.value}'
    Font / icon: '{color.font.indigo-base50.value}'
  - Estado: '**Disabled**'
    Fill: 'ligthen($default-value, 65%)'
    Border: 'ligthen($default-value, 65%)'
    Font / icon: 'ligthen($default-value, 65%)'
  - Estado: '**Open item default**'
    Fill: '{color.root.ui.support.white.value}'
    Border: '-'
    Font / icon: '{color.font.indigo-base.value}'
  - Estado: '**Open item hover**'
    Fill: 'shade($white, 25%)'
    Border: '-'
    Font / icon: '{color.font.indigo-base.value}'

```
### **3.4. Layer**

```table
span: 6
columns:
  - Estado
  - Fill
  - Border
  - Layer
rows:
  - Estado: '**Open**'
    Fill: '{color.root.ui.neutral.white.value}'
    Border: '{color.root.ui.opacity.transparent.value}'
    Layer: '{layer.floating.value}'
```
