```table
span: 6
columns:
  - Type
  - Component
  - Version
  - Sketch library
  - Token
  - React
rows:
  - Type: '**Primitive**'
    Component: color
    Version: v1.0
    Sketch library: ✔️
    Token: ✔️ 
    React:
  - Type:
    Component: typography
    Version: v1.0
    Sketch library: ✔️
    Token: ✔️ 
    React:
  - Type:
    Component: spacing
    Version: v1.1
    Sketch library: ✔️
    Token: ✔️ 
    React:
  - Type:
    Component: layer
    Version: v1.1
    Sketch library: ✔️
    Token: ✔️ 
    React:
  - Type:
    Component: builder
    Version: v1.1
    Sketch library: ✔️
    Token: ✔️ 
    React:
  - Type:
    Component: icons
    Version: v1.2
    Sketch library: ✔️ 
    Token:  
    React:
  - Type: '**Controls**'
    Component: button-primary (sm, md, lg, xl)
    Version: v1.2
    Sketch library: ✔️ 
    Token:
    React:
  - Type:
    Component: button-secondary (sm,. md. lg, xl)
    Version: v1.2
    Sketch library: ✔️ 
    Token:
    React:
  - Type:
    Component: icon-button (sm, md, lg, xl)
    Version: v1.2
    Sketch library: ✔️ 
    Token:
    React:
  - Type:
    Component: text-input (sm, md, lg)
    Version: v1.2
    Sketch library: ✔️ 
    Token:
    React:
  - Type:
    Component: text-input-quote
    Version: v1.2
    Sketch library: ✔️ 
    Token:
    React:
  - Type:
    Component: checkbox-dark (3x, 2xs, xs)
    Version: v1.2
    Sketch library: ✔️ 
    Token:
    React:
  - Type:
    Component: checkbox-light (3xs, 2xs, xs)
    Version: v1.2
    Sketch library: ✔️ 
    Token:
    React:
  - Type:
    Component: radio-button (3xs, 2xs, xs)
    Version: v1.3
    Sketch library: ✔️ 
    Token:
    React:
  - Type:
    Component: dropdown simple (sm, md, lg)
    Version: v1.4
    Sketch library: ✔️ 
    Token:
    React:
  - Type:
    Component: dropdown multi-select (sm, md, lg)
    Version: v1.4
    Sketch library: ✔️ 
    Token:
    React:
  - Type:
    Component: toggles (3x, 2xs, xs)
    Version: v1.0.5
    Sketch library: ✔️ 
    Token:
    React:
```