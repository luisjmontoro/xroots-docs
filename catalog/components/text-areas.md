## Definición

> **Los text areas son elementos de la interfaz que permiten a los usuarios interactuar introduciendo datos. Normalmente forman parte de los formularios, aunque también podemos encontrarlos por separado (por ejemplo: la caja de búsqueda). Siempre van acompañados de un input que ofrece al usuario la posibilidad de completar la acción.**

## Uso

```table
span: 5
columns:
  - Tipo
  - Descripción y uso
rows:
  - Tipo: '**Text Input**'
    Descripción y uso: Campo de texto para que el usuario introduzca una palabra o una frase corta.
  - Tipo: '**Text Area**'
    Descripción y uso: Campo de texto para que el usuario introduzca un párrafo.
``` 

## Composición

El componente text input cuenta en su composición con elementos diferentes según la casuística. Los **elementos en negrita** son los mínimos necesarios para considerar el componente text input como tal:

- **Text label**
- Help text
- **Input (w/ placeholder)**
- Support message (solo visible en los diferentes estados de validación.


```image
    span: 5
    src: "https://gitlab.com/luisjmontoro/xroots-docs/raw/63f9e8d412872af70420ae7832aa5b0ac989e082/catalog/static/img/component-textarea-default.png"
    plain: true
    description: "_Ejemplo: text input / md_"
```

## Propiedades
***

### Sizing

```table
span: 5
columns:
  - size
  - height
  - label font-size
  - placeholder font-size
rows:
  - size: '**default**'
    height: 'Definido por el ancho del input en un ratio de 16:9'
    label font-size: '{size.font.base.value}'
    placeholder font-size: '{size.font.base.value}'

```

### Spacing

Las propiedades de **spacing** son las mismas que las del componente **text input**:

```table
span: 6
columns:
  - size
  - element
  - padding
  - margin
  - spacing
rows:
  - size: '**all**'
    element: label
    padding: '0'
    margin: 0 0 8px 0
    spacing: '{size.spacing.3.value}'
  - size: '**all**'
    element: help text
    padding: '0'
    margin: 0 0 16px 0
    spacing: '{size.spacing.5.value}'
  - size: '**all**'
    element: input (placeholder)
    padding: 0 16px
    margin: 0
    spacing: '{size.spacing.6.value}'
  - size: '**all**'
    element: support message
    padding: 0
    margin: 8px 0 0 0
    spacing: '{size.spacing.3.value}'
  - size: '**all**'
    element: component
    padding: '0'
    margin: 0 0 24px 0
    spacing: '{size.spacing.6.value}'
```

### Color

Las propiedades de **color** son las mismas que las del componente **text input**:

```table
span: 6
columns:
  - Estado
  - Fill
  - Border
  - Font / icon
rows:
  - Estado: '**Default**'
    Fill: '{color.root.ui.neutral.white.value}'
    Border: '{color.root.ui.neutral.base60.value}'
    Font / icon: '{color.font.indigo-base50.value}'
  - Estado: '**Active**'
    Fill: '{color.root.ui.neutral.white.value}'
    Border: '{color.root.ui.neutral.base60.value}'
    Font / icon: '{color.font.indigo-base.value}'
  - Estado: '**Error**'
    Fill: '{color.root.ui.support.white.value}'
    Border: '{color.root.ui.support.error.base.value}'
    Font / icon: '{color.font.indigo-base50.value}'
  - Estado: '**Success**'
    Fill: '{color.root.ui.support.white.value}'
    Border: '{color.root.ui.support.success.base.value}'
    Font / icon: '{color.font.indigo-base50.value}'
  - Estado: '**Disabled**'
    Fill: 'ligthen($default-value, 65%)'
    Border: 'ligthen($default-value, 65%)'
    Font / icon: 'ligthen($default-value, 65%)'

```