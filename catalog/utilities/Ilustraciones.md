> ***Las ilustraciones proporcionan al usuario una correcta comprensión del producto de manera clara y sencilla.*** 
## Racional
En **Xroots** consideramos que las ilustraciones tienen que formar su propio sistema,en el que los elementos que la componen pueden ser personalizables y modulables.

Su función es la de otorgar al usuario información extra y aclaratoria de una manera fácil y visual. Cada ilustración irá enfocada a un producto que a su vez irá representado con los colores estipulados de cada familia.
Las ilustraciones serán escalables para sus diferentes usos y han sido creadas tanto para uso web como para el uso en creatividades.

Estas utilities estarán vivas, ampliandóse cuando se necesite o se requiera.

## Usos
Estos elementos servirán para la realización tanto para la creación de ilustraciones como para la utilización de creatividades.
Los elementos de las ilustraciones deben corresponder a la misma familia, con la excepción de poder combinar los elementos de cada familia con los elementos genéricos (elementos morados)

## Elementos
Todos estos elementos serán escalables a cualquier tamaño manteniendo una coherencia entre ellos. El tamaño en el que partimos estará incrustado en un contenedor de 565px x 515px
##### *Paleta de colores*
```color-palette|horizontal
colors:
   - {value: "#3E2575"}
   - {value: "#745CF4"}
   - {value: "#A282D9"}
   - {value: "#E3C0E0"}
   - {value: "#BCBCD9"}
   - {value: "#E3E3ED"}
   - {value: "#D4D4F5"}
   - {value: "#74747C"}
```
### Edificios
Estos elementos, serán la base de todas las ilustraciones, en ellos actuarán los productos, por lo que es importante que éstos estén identificados de alguna manera.
##### **Oficinas**
```image
   plain: true
   span: 3
   src: "https://gitlab.com/luisjmontoro/xroots-docs/raw/6500f7ebea67afd9ebc1969c55bc9675ff40b66b/catalog/static/img/Office.png"
```
##### **Locales normales**
```image
   plain: true
   span: 3
   src: "https://gitlab.com/luisjmontoro/xroots-docs/raw/6500f7ebea67afd9ebc1969c55bc9675ff40b66b/catalog/static/img/shops-normal.png"
```

##### **Locales específicos**
```image
   plain: true
   span: 5
   src: "https://gitlab.com/luisjmontoro/xroots-docs/raw/6500f7ebea67afd9ebc1969c55bc9675ff40b66b/catalog/static/img/shops.png"
```
##### **Viviendas**
```image
   plain: true
   span: 5
   src: "https://gitlab.com/luisjmontoro/xroots-docs/raw/6500f7ebea67afd9ebc1969c55bc9675ff40b66b/catalog/static/img/build2.png"
```

### Mobiliario urbano
Aquí se encuentran elementos extras como farolas, bancos, etc.
##### **Farola**
```image
   plain: true
   span: 5
   src: "https://gitlab.com/luisjmontoro/xroots-docs/raw/6500f7ebea67afd9ebc1969c55bc9675ff40b66b/catalog/static/img/streetlight.png"
```
##### **Bancos**
```image
   plain: true
   span: 5
   src: "https://gitlab.com/luisjmontoro/xroots-docs/raw/655e042c7d1d30258f7c11e41366cac629b75cbb/catalog/static/img/grey.png"
```
##### **Carreteras y vehículos**
```image
plain: true
span: 4
src: "https://gitlab.com/luisjmontoro/xroots-docs/raw/6500f7ebea67afd9ebc1969c55bc9675ff40b66b/catalog/static/img/car.png"
Description: Las carreteras tendrán la función de nexo entre los diferentes edificios y locales, dando un aspecto de ciudad a las ilustraciones.
```

```image
plain: true
span: 3
src: "https://gitlab.com/luisjmontoro/xroots-docs/raw/6500f7ebea67afd9ebc1969c55bc9675ff40b66b/catalog/static/img/roads.png"
```
##### **Arboles**
```image
plain: true
span: 4
src: "https://gitlab.com/luisjmontoro/xroots-docs/raw/6500f7ebea67afd9ebc1969c55bc9675ff40b66b/catalog/static/img/trees.png"
```
### Antenas
Este elemento se incluirá en los edificios que representen la entrada y salida de datos (wifi por ejemplo).
```image
plain: true
span: 3
src: "https://gitlab.com/luisjmontoro/xroots-docs/raw/6500f7ebea67afd9ebc1969c55bc9675ff40b66b/catalog/static/img/antenna.png"
```
### Personas
Se han creado diferentes tipos de personas dependiendo de la acción en la que se incluyan, por ejemplo:
En **XUCom** será necesario incluir a varias personas en diferentes situaciones de localización.
```image
plain: true
span: 5
src: "https://gitlab.com/luisjmontoro/xroots-docs/raw/6500f7ebea67afd9ebc1969c55bc9675ff40b66b/catalog/static/img/people.png"
```
### Composición de ilustraciones complejas

Los assets vistos anteriormente, pueden combinarse con otros assets no genéricos como los que se encuentran en las ilustraciónes de la ficha de producto. Ejemplo:
```image
plain: true
span: 5
src: "https://gitlab.com/luisjmontoro/xroots-docs/raw/6500f7ebea67afd9ebc1969c55bc9675ff40b66b/catalog/static/img/cloud.png"
```
```image
plain: true
span: 5
src: "https://gitlab.com/luisjmontoro/xroots-docs/raw/8ebe81c5b2c427c3ac03318ff03c49ee11ffc7bd/catalog/static/img/other-assets1.png"
```
```image
plain: true
span: 5
src: "https://gitlab.com/luisjmontoro/xroots-docs/raw/8ebe81c5b2c427c3ac03318ff03c49ee11ffc7bd/catalog/static/img/other-assets2.png"
```
### Otros elementos
Estos elementos harán referencia de alguna manera a la familia que corresponda, dándoles la distinción entre productos y familias.

###### Líneas de conexión:
Las líneas de conexión entre edificios indicarán intercomunicación entre ellos de una manera a elegir (combinánndolos con otros elementos como los puntos)
Estas líneas tendrán un grosor de 3 px con un radius que puede variar de 1 a 20 px máximo.

###### Circulos de conexión:
Indican que entre las conexiones hay datos o algún elemento que se comparte entre ellos. Tamaño 12x12px

###### Otros assets no genéricos:
En ocasiones pueden encontrarse otros elementos que den sentido a la ilustración o utilidad del producto. Esos assets serán generados para dicha utilidad, pudiéndose replicar para otras ilustraciones para mayor sentido y unificación de las mismas.
