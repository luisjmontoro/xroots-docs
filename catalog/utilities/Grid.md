> ***El grid proporciona al usuario una forma guía para encontrar el contenido que necesita***

En **XRoots** proponemos un grid en base a **8pt**.
Con él, somos capaces de construir un layout flexible y agnóstico a nivel de dispositivo, capaz de adaptarse a cualquier resolución con unas sencillas consideraciones.


## Breakpoints y estructura del grid para diseño

```table
span: 3
columns:
  - Size
  - Columns
  - Gutters
  - Min. outer margin
rows:
  - Size: '**S (320px)**'
    Columns: '2'
    Gutters: 16px
    Min. outer margin: 5%
  - Size: '**M (768px)**'
    Columns: '6'
    Gutters: 16px
    Min. outer margin: 5%
  - Size: '**L (1200px)**'
    Columns: '12'
    Gutters: 24px
    Min. outer margin: 3%
  - Size: '**XL (1440px)**'
    Columns: '12'
    Gutters: 24px
    Min. outer margin: 3%


```

## Breakpoints y estructura del grid para desarrollo

```hint
A pesar de utilizar el mismo grid, por definición técnica, desarrollo necesita la conversión del breakpoint de 1200px a **1024px**. Esto garantiza una correcta visualización en todos los dispositivos independientemente de su orientación y no afecta para nada al proceso de diseño.
```

```table
span: 3
columns:
  - Size
  - Columns
  - Gutters
  - Min. outer margin
rows:
  - Size: '**xs (0)**'
    Columns: '2'
    Gutters: 16px
    Min. outer margin: 5%
  - Size: '**sm (576px)**'
    Columns: '2'
    Gutters: 16px
    Min. outer margin: 5%
  - Size: '**md (768px)**'
    Columns: '6'
    Gutters: 16px
    Min. outer margin: 5%
  - Size: '**lg (1024px)**'
    Columns: '12'
    Gutters: 24px
    Min. outer margin: 3%
  - Size: '**xl (1440px)**'
    Columns: '12'
    Gutters: 24px
    Min. outer margin: 3%

```
## Márgenes

El contenedor de 12 columnas del grid no contempla un ancho máximo, sino que ocupa el 100% del ancho contando con los **márgenes**. 

En dispositivos de más de 768px el margen ha de ser **como mínimo** del 5% por cada lado.

En dispositivos de menos de 768px, el mismo margen ha de ser **como mínimo** del 3% por cada lado.


## Layout
### 1200px
```image
src: "https://gitlab.com/luisjmontoro/xroots-docs/raw/master/catalog/static/img/grid-12-col.png"
plain: true
```
### 768px
```image
span: 4
src: "https://gitlab.com/luisjmontoro/xroots-docs/raw/master/catalog/static/img/grid-6-col.png"
plain: true
```
### 320px
```image
span: 2
src: "https://gitlab.com/luisjmontoro/xroots-docs/raw/master/catalog/static/img/grid-2-col.png"
plain: true
```
