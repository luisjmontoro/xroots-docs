## Catalog

Catalog es la herramienta que usamos para documentar nuestro DSL XRoots. Para realizar contribuciones a la documentación es necesario instalar Catalog en la máquina local así como tener clonado el repositorio de la documentación.

## Instalación

Para correr Catalog en una máquina local es necesario tener instaladas varias dependencias:

### Homebrew

Copiar y pegar este comando en la terminal de mac OSX para instalar Homebrew: 

```code
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

### NodeJS

Descargar e instalar la última versión estable de NodeJS:

[Descargar NodeJS](https://nodejs.org/es/download/)

### Yarn

Instalar yarn copiando y pegando este comando en la terminal. Para ello, antes se debe tener instalado Homebrew (ver arriba)

```code
brew install yarn
```

## Repositorio

Tenemos una versión siempre actualizada del repositorio de esta documentación en [Gitlab](https://gitlab.com/luisjmontoro/xroots-docs?nav_source=navbar)

Clonar el repositorio vía SSH: 

```code
git clone git@gitlab.com:luisjmontoro/xroots-docs.git
```

## Levantar Catalog en una máquina local

```code
npm run catalog-start
npm run catalog-build
```

```hint
Para levantar Catalog en una máquina local hay que tener previamente instaladas y actualizadas todas sus dependencias
```
