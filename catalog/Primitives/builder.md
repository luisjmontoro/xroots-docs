> ***Nuestro builder es una librería de formas primitivas cuyo propósito es construir el 100% de los átomos necesarios en los componentes del DSL.***

## El concepto

El **builder**, en su formato de **librería de Sketch**, ofrece un sistema de formas y elementos subatómicos que sirven para construir cualquiera de los elementos atómcos de interfaz de los componentes (a excepción de los iconos).

Entre sus formas encontramos:

- Fill
- Border
- Layer
- State

Cada una de las categorías anteriores se subdivide, según necesidad, en elementos con diferentes border-radius:

- 100%
- 8px 
- 4px
- 2px 
- 0 (squared)

### Detalles

```table
span: 3
columns:
  - Categoría
  - Descripción y uso
rows:
  - Categoría: '**Fill**'
    Descripción y uso: Agrupa los elementos sólidos con relleno de la UI.
  - Categoría: '**Border**'
    Descripción y uso: Agrupa los bordes de los componentes de nuestra UI.
  - Categoría: '**Layer**'
    Descripción y uso: Agrupa los grados de elevación definidos en **XRoots**
  - Categoría: '**State**'
    Descripción y uso: Agrupa los diferentes posibles estados en formato de overlay para su uso como capa en Sketch.
  ```

  ## Fill

  ```image
src: "https://gitlab.com/luisjmontoro/xroots-docs/raw/de92ff7f9acce9b7f1eee55d5fa2aeea63f89e88/catalog/static/img/fill-example.png"
plain: true
description: "_Ejemplo: shapes / fill / left / 100%; 8px; 4px; 2px; 0_"
```

  Fill agrupa los rellenos de los componentes de la UI. En el **builder** por defecto se anidan según su border-radius (0, 2px 4px, 8px, 100%) y la orientación de los mismos (top, right, bottom, left).

  Tamaño del artboard: 80x80.

  ## Border

  ```image
src: "https://gitlab.com/luisjmontoro/xroots-docs/raw/de92ff7f9acce9b7f1eee55d5fa2aeea63f89e88/catalog/static/img/border-example.png"
plain: true
description: "_Ejemplo: shapes / border / left / 100%; 8px; 4px; 2px; 0 / 2px width_"
```

  Border agrupa todos los bordes que pintaremos en nuestra interfaz. Se clasifican de la misma manera que los fills y poseen las mismas características de anidación, con la excepción de que los bordes poseen **una capa más de personalización**: el ancho (border-width de 1px, 2px o 3px).

  Tamaño del artboard: 78x78.

  ## Layer

  ```image
  src: "https://gitlab.com/luisjmontoro/xroots-docs/raw/de92ff7f9acce9b7f1eee55d5fa2aeea63f89e88/catalog/static/img/layer-example.png"
  plain: true
  description: "_Ejemplo: shapes / layer / left / 100%; 8px; 4px; 2px; 0 / floating_"
  ```

  De todos los estados de elevación definidos en Layer, nuestro builder solo cuenta con dos de ellos: raised y floating, ya que son los que muestran estilos visuales que posicionan su grado de elevación.

  Tamaño del artboard: 76x76.

  ## State

  ```image
src: "https://gitlab.com/luisjmontoro/xroots-docs/raw/de92ff7f9acce9b7f1eee55d5fa2aeea63f89e88/catalog/static/img/state-example.png"
plain: true
description: "_Ejemplo: shapes / state / left / 100%; 8px; 4px; 2px; 0 / :hover_"
```

  State agrupa tres posibles estados (:hover, pressed and disabled) en formato de capas de overlay que se posicionan sobre los elementos.

  Para construirlos, seguimos la misma lógica de variables lighten and darken con la que formamos nuestra paleta de color UI, salvo que la aplicamos sobre la capa de overlay.

  Tamaño del artboard: 72x72.

  ## Sketch overrides

  Para asegurar un sistema de overrides sostenible en los elementos compuestos por las partículas subatómicas del Builder, hemos dotado a los artboards de cada categoría (fill, border, layer y state) de diferentes medidas.

  De este modo, para realizar un override en Sketch, la aplicación nos ofrecerá elementos de la misma categoría:

  ## Contribuciones

  A la hora de realizar cualquier contribución al **builder** hay que tener muy en cuenta dos cosas:

  - **Tamaños de los artboards:** se deben respetar los tamaños de los artboards de los elementos ya especificados (fill, border, layer, state). Para nuevos elementos, se deberá seguir la escala reduciendo los valores (80, 76, 78...) para evitar contaminar los overrides.

  - **Resizing attributes:** para evitar deformaciones en los elementos, hay que tener especial cuidado a la hora de definir los valores de resizing y anclaje. Es recomendable ver cómo están tratados el resto de los elementos a este respecto.