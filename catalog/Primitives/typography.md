>***Arte de disponer correctamente el material (...), de acuerdo con un propósito específico: el de colocar las letras, repartir el espacio y organizar los tipos con vistas a prestar al lector la máxima ayuda para la comprensión del texto escrito verbalmente.***
>
>Stanley Morison

En **XRoots** consideramos la tipografía como un elemento clave y de vital importancia en el diseño. Su función jerárquica y clarificadora nos ha obligado a desarrollar una escala propia a la hora de definir su uso.

## Typeface
### Lato


```
font-family: Lato, sans-serif;
```
```image
src: "https://ucfb40b94b0ff7ccd70efdee5cbe.previews.dropboxusercontent.com/p/thumb/AAWUptBmpJSpNXi0lbM94OvNeMBrFk7RqmvgAQlQq8bM-Ll1YsJjg3OXr-GymRDlYUCpbpa_cAEjwzugB_rT-iTMANyFhep2KFEy9DnSr4vVcjs3dblYd30sB5QsTm0Dl6Z4lzeyYOkhBttTsq9K7uG9PTPgNSo2vjAU2usiwCUDwTSHiKcbIcVSICsqe4GS-zn5VZYY-gRRLCVaJ6Cqw88DJHSmbCGdhsZuqAZytF2bTtigjzUVoqCKRBoNvik9AjIEZpmKxBrJOhQ2o9u3yXyryBRoixlvqXuBhkwZY2Amxu-uaVorXXfiSiEloi49gLie9HrfsFQaoSrL7_MIBHht9m3-IFY9-xlLrIY3utzrg8NDOFpJLyfnh0RHqN5h_DQFaOa1O1_9z2Fq6yhSSPs9/p.png?size_mode=5"
plain: true
```

[Descargar Lato @ Google fonts](https://fonts.google.com/specimen/Lato)

## TypeScale

En **XRoots** hemos decidido escalar la tipografía con una lógica práctica. En lugar de seguir un ratio equivalente progresivo (como el ya conocido Modular Scale), hemos establecido una base de 16px = 1rem y un ratio de partida “Minor Third (1.2)” para, posteriormente, escalar la tipografía en intervalos personalizados (steps) a raíz de una fórmula matemática en lugar de seguir estrictamente el ratio.

La razón de ser de este planteamiento es conseguir un enfoque práctico y evitar una cantidad de variables poco útiles entre los tamaños estándar de la UI y los headers.

### Fórmula

```hint
10px + 2
```

```table
span: 3
columns:
  - Step
  - Px
  - Rem
rows:
  - Step: '-3'
    Px: 10
    Rem: 0.625rem
  - Step: '-2'
    Px: 12=10+2
    Rem: 0.75rem
  - Step: '-1'
    Px: 14=12+2
    Rem: 0.875rem
  - Step: '0'
    Px: 16=14+2
    Rem: 1rem
  - Step: '1'
    Px: 18=16+2
    Rem: 1.125rem
  - Step: '2'
    Px: 20=18+2
    Rem: 1.25rem
  - Step: '3'
    Px: 24=20+2+2
    Rem: 1.5rem
  - Step: '4'
    Px: 28=24+2+2
    Rem: 1.75rem
  - Step: '5'
    Px: 32=28+2+2
    Rem: 2rem
  - Step: '6'
    Px: 36=32+2+2
    Rem: 2.25rem
  - Step: '7'
    Px: 40=36+2+2
    Rem: 2.5rem
  - Step: '8'
    Px: 46=40+2+2+2
    Rem: 2.875rem
  - Step: '9'
    Px: 52=46+2+2+2
    Rem: 3.25rem
  - Step: '10'
    Px: 58=52+2+2+2
    Rem: 3.625rem
  - Step: '11'
    Px: 64=58+2+2+2
    Rem: 4rem
  - Step: '12'
    Px: 72=64+2+2+2+2
    Rem: 4.5rem
  - Step: '13'
    Px: 80=72+2+2+2+2
    Rem: 5rem
  - Step: '14'
    Px: 90=80+2+2+2+2+2
    Rem: 5.625rem

```

```image
src: "https://previews.dropbox.com/p/thumb/AAV94fo7PfE9HEflTR9j4eQ2V_CgsviQTaRW4_Iv4aXiuUEpTnL7QhnoSS0Xc3y1RIHtgSVBqEDj9TBxCcX9huzOLV7UK9B9Y9oYVwtjz71kKNJAgzYwT6XqwIu3aVVCIvAjXfsM79BYt4vL_yOB7foM-te2AQUgMJ-1sAdtOe6lQgkyo_T1ngAPTB2Q0WSh_VYr3tSXqrfLtlizYVk4U5VseFrl-xjF1b_ZWrpRPuMxCONtWvaz-cu0fjA282LNHuiXCG_OrkIx_oMRCxxzZJcme_5MWQdtxFruEh2jPbSV06T8UhYICEnbDdymtWDSfTx-Nm7cmwTcrBIbq5xmMkcefQP4o5cQ-5tSsIIQbJseZg/p.png?size_mode=5"
plain: true
```




## Responsive balance
Nuestra fórmula respeta la relación de la proporción entre los elementos en diferentes dispositivos y resoluciones.



```table
columns:
  - Step
  - Px
  - Rem
  - Desktop
  - Tablet
  - Mobile
rows:
  - Step: '-3'
    Px: 10
    Rem: 0.625rem
    Desktop: others
    Tablet: others
  - Step: '-2'
    Px: 12=10+2
    Rem: 0.75rem
    Desktop: legal
    Tablet: legal
    Mobile: legal, others
  - Step: '-1'
    Px: 14=12+2
    Rem: 0.875rem
    Desktop: caption
    Tablet: caption
    Mobile: caption
  - Step: '0'
    Px: 16=14+2
    Rem: 1rem
    Desktop: body
    Tablet: body
    Mobile: body
  - Step: '1'
    Px: 18=16+2
    Rem: 1.125rem
    Tablet: h6
    Mobile: h6
  - Step: '2'
    Px: 20=+2+2
    Rem: 1.25rem
    Desktop: h6
    Tablet: h5
    Mobile: h5
  - Step: '3'
    Px: 24=20+2+2
    Rem: 1.5rem
    Desktop: h5
    Tablet: h4
    Mobile: h4
  - Step: '4'
    Px: 28=24+2+2
    Rem: 1.75rem
    Desktop: h4
    Tablet: h3
    Mobile: h3
  - Step: '5'
    Px: 32=28+2+2
    Rem: 2rem
    Desktop: h3
    Tablet: h2
    Mobile: h2
  - Step: '6'
    Px: 36=32+2+2
    Rem: 2.25rem
    Mobile: h1
  - Step: '7'
    Px: 40=36+2+2
    Rem: 2.5rem
    Tablet: h1
  - Step: '8'
    Px: 46=40+2+2+2
    Rem: 2.875rem
    Desktop: h2
    Mobile: display3
  - Step: '9'
    Px: 52=46+2+2+2
    Rem: 3.25rem
    Tablet: display3
    Mobile: display2
  - Step: '10'
    Px: 58=52+2+2+2
    Rem: 3.625rem
    Desktop: h1
    Tablet: display2
    Mobile: display1
  - Step: '11'
    Px: 64=58+2+2+2
    Rem: 4rem
    Desktop: display3
    Tablet: display1
  - Step: '12'
    Px: 72=64+2+2+2+2
    Rem: 4.5rem
    Desktop: display2
  - Step: '13'
    Px: 80=72+2+2+2+2
    Rem: 5rem
    Desktop: display1
  - Step: '14'
    Px: 90=80+2+2+2+2+2
    Rem: 5.625rem

```
## Type Leading
El leading (line-height) influye directamente en la legibilidad del texto. En **XRoots**, el leading viene definido por el propio tamaño de la fuente a través de un ratio estándar de 1:1.5 (rem), salvo en los headers, que, por su morfología, el ratio es menor: 1:1.125 (rem).

```table
span: 3
columns:
  - Line-height
  - Ratio
rows:
  - Line-height: 'UI'
    Ratio: '1:1.5'
  - Line-height: 'Headers'
    Ratio: '1:1.125'
```

## CSS Classes
Para poder reutilizar los estilos de texto definidos en nuestra escala sin aludir directamente a las etiquetas **HTML**, hemos diseñado un sistema de **clases** que permitirá sobrescribir los estilos por defecto de una etiqueta sin estar vinculada en código a la misma. Por ejemplo, podremos utilizar los estilos de texto de un tag **h1** si lo necesitamos, sin necesidad de pintar, en código, un h1.

```table
columns:
  - Step
  - Px
  - Rem
  - Desktop
  - Class
rows:
  - Step: '-3'
    Px: 10
    Rem: 0.625rem
    Desktop: others
    Class: .others
  - Step: '-2'
    Px: 12=10+2
    Rem: 0.75rem
    Desktop: legal, others
    Class: .legal
  - Step: '-1'
    Px: 14=12+2
    Rem: 0.875rem
    Desktop: caption
    Class: .caption
  - Step: '0'
    Px: 16=14+2
    Rem: 1rem
    Desktop: body
    Class: .body-font
  - Step: '1'
    Px: 18=16+2
    Rem: 1.125rem
    Class: .description
  - Step: '2'
    Px: 20=+2+2
    Rem: 1.25rem
    Desktop: h6
    Class: .heading-6
  - Step: '3'
    Px: 24=20+2+2
    Rem: 1.5rem
    Desktop: h5
    Class: .heading-5
  - Step: '4'
    Px: 28=24+2+2
    Rem: 1.75rem
    Desktop: h4
    Class: .heading-6
  - Step: '5'
    Px: 32=28+2+2
    Rem: 2rem
    Desktop: h3
    Class: .heading-3
  - Step: '6'
    Px: 36=32+2+2
    Rem: 2.25rem
  - Step: '7'
    Px: 40=36+2+2
    Rem: 2.5rem
  - Step: '8'
    Px: 46=40+2+2+2
    Rem: 2.875rem
    Desktop: h2
    Class: .heading-2
  - Step: '9'
    Px: 52=46+2+2+2
    Rem: 3.25rem
  - Step: '10'
    Px: 58=52+2+2+2
    Rem: 3.625rem
    Desktop: h1
    Class: .heading-1
  - Step: '11'
    Px: 64=58+2+2+2
    Rem: 4rem
    Desktop: display3
    Class: .display-3
  - Step: '12'
    Px: 72=64+2+2+2+2
    Rem: 4.5rem
    Desktop: display2
    Class: .display-2
  - Step: '13'
    Px: 80=72+2+2+2+2
    Rem: 5rem
    Desktop: display1
    Class: .display-1
  - Step: '14'
    Px: 90=80+2+2+2+2+2
    Rem: 5.625rem

```

## Desktop scale example

```type
{
  "headings": [58,46,32,28,24,20],
  "font": "Lato",
  "color": "#2F3343"
}
```
## Tablet scale example

```type
{
  "headings": [40,32,28,24,20,18],
  "font": "Lato",
  "color": "#2F3343"
}
```
## Mobile scale example

```type
{
  "headings": [36,32,28,24,20,18],
  "font": "Lato",
  "color": "#2F3343"
}
```