Las capas (layers) conforman la base del ecosistema de los componentes, son la superficie sobre las que estos se agrupan. Siendo todas del mismo grosor, poseen diferentes medidas y grados de elevación entre sí.

Por defecto, **XRoots** cuenta con cuatro niveles de elevación entre capas o entre los propios componentes con respecto a la base: disabled (-1) // resting (0) // raised (2) // floating (8).

El nivel de elevación de cada capa viene indicado por su sombra:

```image
span: 3
src: "https://gitlab.com/luisjmontoro/xroots-docs/raw/1425c2b74e64e9f084ab836f867dae584da2e5f5/catalog/static/img/layer-types.png"
plain: true
```


## Tipos



```table
span: 3
columns:
  - Layer name
  - Elevation
  - Box-shadow
rows:
  - Layer name: Disabled
    Elevation: '-1'
    Box-shadow: none
  - Layer name: Resting
    Elevation: '0'
    Box-shadow: none
  - Layer name: Raised
    Elevation: '2'
    Box-shadow: 0 2px 4px 0 rgba(0,0,0,0.15);
  - Layer name: Floating
    Elevation: '8'
    Box-shadow: 0 8px 10px 0 rgba(0,0,0,0.15);
  ```

  ### Cómo calcular el box-shadow

  ```code
  y = elevation  -->  blur = y+2
  ```

```image
span: 5
src: "https://gitlab.com/luisjmontoro/xroots-docs/raw/68955eb024b542a6b09ee053e5d4d522da160fd0/catalog/static/img/elevation-scale-chart.png"
plain: true
```