## Cocepto
Los iconos agregan interés visual y reducen la carga cognitiva mediante el uso de  pictogramas sencillos que mantienen alguna relación de dependencia con acciones y/o elementos más complejos, optimizando así las ejecuciones de tareas para los usuarios que interactúan con la interfaz.

```image
plain: true
span: 6
src: "https://gitlab.com/luisjmontoro/xroots-docs/raw/558a7600a47abd81964c5f6b097304276d35f697/catalog/static/img/icon-icons.png"
description:
```

## Retícula

La retícula cuadrada es el tejido subyacente de todos los íconos de IBM y se usa como base para determinar el grosor de la línea, la proporción, la forma y el posicionamiento en todo el conjunto de íconos. La cuadrícula ayuda a guiar las decisiones de diseño, lo que garantizará un enfoque unificado, pero lo que es más importante, permitirá la flexibilidad en la creación de la forma adecuada necesaria para comunicar la idea correcta.

```image
plain: true
span: 6
src: "https://gitlab.com/luisjmontoro/xroots-docs/raw/7a19d7bd119d7c325502b7b2f41c65766d0dd7d9/catalog/static/img/icon-construction.png"
description:
```



## Área y método de construcción

Los iconos deben construirse a partir de una mesa de trabajo de **16x16 píxeles** en fondo transparente y dejando un área de reserva de **2 píxeles** por cada borde, empleando así un área activa de construcción de **12x12 píxeles**. Se debe tomar en cuenta que línea base de construcción debe ser de **1 pixel** y el radio de las mismas **1 píxel**, en caso de insertar formas, debe colocarse el **borde interno**.
Posteriormente y una vez finalizada la construcción básica del icono, es importante seguir los siguientes pasos:

- Unificar todos los trazos que componen el icono en una única capa.
- Expandir el icono (flatten).
- Renombrar la capa única como "fill".
- Agregar estilo de color "a.color/ a.ui / b.neutral / a.base" de la DSL de color.
- Reajustar el tamaño del icono al tamaño total de la mesa de trabajo (16 pixeles) manteniendo la proporción del mismo.

 **Los procedimientos anteriores garantizarán que los iconos conservarán las características como overrides de color, proporciones y morfología deseadas al ser exportados, manteniendo también su legibilidad teniendo en cuenta el tamaño míninmo de los mismos.** 

```image
plain: true
span: 5

src: "https://gitlab.com/luisjmontoro/xroots-docs/raw/558a7600a47abd81964c5f6b097304276d35f697/catalog/static/img/icons-area.png"
description:
```



## Alineación

Los íconos se modifican linealmente  a diferentes tamaños (16px - 24px - 32px - 64px). Podemos utilizar la cuadrícula como guía básica para ajustar el icono en su lugar. Se recomienda realizar ajustes precisos durante el proceso para admitir la mejor forma y detalles que se quieran lograr.

```image
plain: true
span: 5
src: "https://gitlab.com/luisjmontoro/xroots-docs/raw/558a7600a47abd81964c5f6b097304276d35f697/catalog/static/img/icons-validaciones.png"
description:
```


- Debemos alinear los elementos del diseño a la cuadrícula al momento de la construcción básica del icono, la única excepción permitida será el posicionamiento de cualquier línea que componga un icono y que así lo requiera para mantener su posición central y tamaño total en la mesa de trabajo, como por ejemplo en la construcción de una flecha. En este caso la línea (1px) deberá estar posicionada en medio de dos pixeles por igual es decir 0,5 px.

- Evitaremos  los puntos decimales aleatorios en las coordenadas x e y en el área total que ocupa el icono.



## Iconos Complejos
Los iconos complejos se utilizan como ilustraciones para acompañar textos informativos que se quieran resaltar como por ejemplo características o beneficios de un producto.
```image
plain: true
span: 5
src: "https://gitlab.com/luisjmontoro/xroots-docs/raw/a9ea2fcd360dbc87c7d9a65c5be221001bd9d583/catalog/static/img/icons_complex.png"
description:
```


## Grid de construcción
La construcción de los iconos complejos parte del racional de las ilustraciones del sistema de diseño en una mesa de trabajo de 64x64 píxeles, utilizando figuras isométricas que comuniquen una idea rápida y facil de entender para el usuario. La paleta de color a emplear también será la establecida para las ilustraciones en el sistema de diseño.

```image
plain: true
span: 3
src: "https://gitlab.com/luisjmontoro/xroots-docs/raw/a9ea2fcd360dbc87c7d9a65c5be221001bd9d583/catalog/static/img/isometric_grid.png"
description:
```

