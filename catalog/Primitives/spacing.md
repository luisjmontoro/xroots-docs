> ***Entender el spacing y saber utilizarlo correctamente es uno de los aspectos fundamentales para un buen diseño de producto digital***

Usar el espacio de forma ordenada y consistente en una interfaz genera balance y ritmo, el buen uso del espacio mejora la calidad percibida en la experiencia de usuario.

Nuestros tokens de spacing se utilizan de forma consistente en todos los componentes y el layout. La aplicación de estos Tokens se puede realizar a través de paddings o margins en los componentes y a través de reglas que definen los espacios de separación en los layouts.

Hemos unificado los espaciados propios de los componentes y los del layout creando una única escala para todos. El motivo es que de este modo evitamos valores duplicados y es más fácil referenciar la escala en cualquier instancia.

En nuestros 12 saltos cubrimos tanto los spacing de componentes como los que necesitemos para garantizar un ritmo vertical de layout coherente y consistente.

```table
span: 3
columns:
  - Name
  - px
  - rem
  - Token
rows:
  - Name: '**spacing-1**'
    px: '2'
    rem: 0.125
    Token: '{size.spacing.1.value}'
  - Name: '**spacing-2**'
    px: '4'
    rem: 0.25
    Token: '{size.spacing.2.value}'
  - Name: '**spacing-3**'
    px: '8'
    rem: 0.5
    Token: '{size.spacing.3.value}'
  - Name: '**spacing-4**'
    px: '16'
    rem: 1
    Token: '{size.spacing.4.value}'
  - Name: '**spacing-5**'
    px: '24'
    rem: 1.5
    Token: '{size.spacing.5.value}'
  - Name: '**spacing-6**'
    px: '32'
    rem: 2
    Token: '{size.spacing.6.value}'
  - Name: '**spacing-7**'
    px: '40'
    rem: 2.5
    Token: '{size.spacing.7.value}'
  - Name: '**spacing-8**'
    px: '48'
    rem: 3
    Token: '{size.spacing.8.value}'
  - Name: '**spacing-9**'
    px: '64'
    rem: 5
    Token: '{size.spacing.9.value}'
  - Name: '**spacing-10**'
    px: '96'
    rem: 6
    Token: '{size.spacing.10.value}'
  - Name: '**spacing-11**'
    px: '160'
    rem: 10
    Token: '{size.spacing.11.value}'


```

###  Spacing table
```image
src: "https://gitlab.com/luisjmontoro/xroots-docs/raw/master/catalog/static/table-spacing.png"
plain: true
```