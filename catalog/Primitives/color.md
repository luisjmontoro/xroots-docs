> ***Un único color excita, mediante una sensación específica, la tendencia a la universalidad. En esto reside la ley fundamental de toda la armonía de los colores...***
>
>Goethe, Teoría de los colores.

La paleta de color de **XRoots** está organizada de forma armónica para conseguir abarcar, en dos grandes grupos, todas las necesidades y usos de nuestros productos, así como crear patrones de diseño significativos a la par que emocionales.

**XRoots** divide su paleta de colores en dos grandes grupos, según su uso: 

1. [Brand](#brand)
2. [UI](#ui)

Cada grupo de colores nace de un color que definimos como 'base' a partir del cual desglosamos, en intervalos de 25% de opacidad, diferentes tonalidades.

Por ejemplo:

```color-palette|span-2
colors:
  - {name: "primary-base", value: "#4E26A6"}
  - {name: "primary-base75", value: "#7A5CBC"}
```


*La paleta de color **UI / Neutral** es la única que, debido a su naturaleza, se desglosa en intervalos de 10% de opacidad en lugar de 25%.*


## Brand
---
- [Usage](#usage)
- [Swatches & tokens](#swatches)

### Usage

El grupo **Brand** de nuestra paleta está dedicado originalmente a los colores propios de la marca. Dentro de **Brand** identificamos tres agrupaciones: 

-**Primary**: color primario de X by Orange.
-**Families**: colores de las distintas familias de productos y servicios de X by Orange.
-**Global**: colores globales de Orange.

### Swatches & tokens

#### Primary


```color-palette
colors:
  - {name: "primary-base", value: "#4E26A6"}
  - {name: "primary-base75", value: "#7A5CBC"}
  - {name: "primary-base50", value: "#A692D2"}
  - {name: "primary-base25", value: "#D3C9E9"}
```
#### Families


```color-palette|span-2
colors:
  - {name: "family1-base", value: "#FFC000"}
  - {name: "family1-base75", value: "#FECF3F"}
  - {name: "family1-base50", value: "#FDDD7D"}
  - {name: "family1-base25", value: "#FBEBBB"}
```
```color-palette|span-2
colors:
  - {name: "family2-base", value: "#FF5F5F"}
  - {name: "family2-base75", value: "#FE8686"}
  - {name: "family2-base50", value: "#FDADAD"}
  - {name: "family2-base25", value: "#FBD3D3"}
```
```color-palette|span-2
colors:
  - {name: "family3-base", value: "#00CEC9"}
  - {name: "family3-base75", value: "#3FDAD6"}
  - {name: "family3-base50", value: "#7DE3E2"}
  - {name: "family3-base25", value: "#BBEFED"}
```
```color-palette|span-2
colors:
  - {name: "family4-base", value: "#08D978"}
  - {name: "family4-base75", value: "#45E299"}
  - {name: "family4-base50", value: "#81E9B9"}
  - {name: "family4-base25", value: "#BDF1D9"}
```

#### Global


```color-palette
colors:
  - {name: "global-base", value: "#FF7900"}
  - {name: "global-dark", value: "#000000"}
  - {name: "global-light", value: "#FFFFFF"}
```

## UI
---
- [Usage](#usage)
- [Swatches & tokens](#swatches)

### Usage

El grupo **UI** de nuestra paleta cubre todas las necesidades de diseño de nuestros componentes de interfaz. Está dividido en varias agrupaciones:

-**Support**: contempla los colores relativos a los mensajes de soporte: *success, warning, error, info*.
-**Neutral**: ofrece una escala de grises en intervalos de 10% de opacidad desde un valor base.
-**Text**: considera los colores de los textos.
-**Link**: acoge los colores de los enlaces y sus diferentes estados.
-**Social**: recoge los colores de diferentes redes o plataformas sociales.

### Swatches & tokens

#### Support

```color-palette|span-2
colors:
  - {name: "success-base", value: "#32C832"}
  - {name: "success-base75", value: "#66D666"}
  - {name: "success-base50", value: "#98E398"}
  - {name: "success-base25", value: "#CCF1CC"}
  - {name: "success-text", value: "#00A000"}
```

```color-palette|span-2
colors:
  - {name: "warning-base", value: "#FFCC00"}
  - {name: "warning-base75", value: "#FFD840"}
  - {name: "warning-base50", value: "#FFE580"}
  - {name: "warning-base25", value: "#FFF2BF"}
  - {name: "warning-text", value: "#BF9800"}
```

```color-palette|span-2
colors:
  - {name: "error-base", value: "#DB4B24"}
  - {name: "error-base75", value: "#E4785A"}
  - {name: "error-base50", value: "#EDA591"}
  - {name: "error-base25", value: "#F5D1C8"}
  - {name: "error-text", value: "#B22600"}
```

```color-palette|span-2
colors:
  - {name: "info-base", value: "#467EF3"}
  - {name: "info-base75", value: "#749EF6"}
  - {name: "info-base50", value: "#A2BEF9"}
  - {name: "info-base25", value: "#D0DEFB"}
  - {name: "info-text", value: "#145CF3"}
```

#### Neutral

```color-palette
colors:
  - {name: "neutral-base", value: "#222222"}
  - {name: "neutral-base90", value: "#383838"}
  - {name: "neutral-base80", value: "#4E4E4E"}
  - {name: "neutral-base70", value: "#646464"}
  - {name: "neutral-base60", value: "#7A7A7A"}
  - {name: "neutral-base50", value: "#B7B7B7"}
  - {name: "neutral-base40", value: "#CCCCCC"}
  - {name: "neutral-base30", value: "#E6E6E6"}
  - {name: "neutral-base20", value: "#F3F3F3"}
  - {name: "neutral-base10", value: "#FAFAFA"}
  - {name: "black", value: "#000000"}
  - {name: "white", value: "#FFFFFF"}
```

#### Text

```color-palette
colors:
  - {name: "text-base", value: "#2F3343"}
  - {name: "text-base75", value: "#636672"}
  - {name: "text-base50", value: "#9799A1"}
  - {name: "text-base25", value: "#CACBCF"}
```

#### Link

```color-palette
colors:
  - {name: "link-default / link-active", value: "#4E26A6"}
  - {name: "link-hover / link-visited", value: "#A692D2"}
```

#### Social

```color-palette
colors:
  - {name: "behance", value: "#1769FF"}
  - {name: "dribbble", value: "#EA4C89"}
  - {name: "dropbox", value: "#0061FF"}
  - {name: "facebook", value: "#3B5998"}
  - {name: "github", value: "#4078C0"}
  - {name: "google-plus", value: "#D34836"}
  - {name: "instagram", value: "#4C5FD7"}
  - {name: "linkedin", value: "#0077B5"}
  - {name: "pinterest", value: "#CB2027"}
  - {name: "skype", value: "#00AFF0"}
  - {name: "slack", value: "#6ECADC"}
  - {name: "spotify", value: "#1ED760"}
  - {name: "twitter", value: "#00B6F1"}
  - {name: "vimeo", value: "#45BBFF"}
  - {name: "youtube", value: "#FF0000"}
```



