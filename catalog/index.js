import React from "react";
import ReactDOM from "react-dom";
import { Catalog, pageLoader } from "catalog";
import style from "./static/style.css";

const pages = [
  {
    path: "/",
    title: "Inicio",
    content: pageLoader(() => import("./inicio.md"))
  },
  {
    path: "/installation",
    title: "Instalación",
    content: pageLoader(() => import("./installation.md"))
  },
  {
  
    title: 'Utilities',
    basePath: '/utilities',
    pages: [
      {
        path: '/grid',
        title: 'Grid & Layout',
        content: pageLoader(() => import("./utilities/grid.md"))
      },
      {
        path: '/ilustraciones',
        title: 'Ilustraciones',
        content: pageLoader(() => import("./utilities/ilustraciones.md"))
      },
      // Other subpages of 'Basics'
    ]
  },
  {
    title: 'Primitives',
    basePath: '/primitives',
    pages: [
      {
        path: '/color',
        title: 'Color',
        content: pageLoader(() => import("./primitives/color.md"))
      },
      {
        path: '/typography',
        title: 'Typography',
        content: pageLoader(() => import("./primitives/typography.md"))
      },
      {
        path: '/spacing',
        title: 'Spacing',
        content: pageLoader(() => import("./primitives/spacing.md"))
      },
      {
        path: '/icons',
        title: 'Icons',
        content: pageLoader(() => import("./primitives/icons.md"))
      },
      {
        path: '/layer',
        title: 'Layer',
        content: pageLoader(() => import("./primitives/layer.md"))
      },
      {
        path: '/builder',
        title: 'Builder',
        content: pageLoader(() => import("./primitives/builder.md"))
      },

      // Other subpages of 'Basics'
    ]
  },
  {
    title: 'Components',
    basePath: '/components',
    pages: [
      {
        path: '/component-status',
        title: 'Component Status',
        content: pageLoader(() => import("./components/status.md"))
      },
      {
        path: '/buttons',
        title: 'Buttons',
        content: pageLoader(() => import("./components/buttons.md"))
      },
      {
        path: '/checkbox',
        title: 'Checkbox',
        content: pageLoader(() => import("./components/checkbox.md"))
      },
      {
        path: '/dropdowns',
        title: 'Dropdowns',
        content: pageLoader(() => import("./components/dropdowns.md"))
      },
      {
        path: '/graphics',
        title: 'Graphics',
        content: pageLoader(() => import("./components/graphics.md"))
      },
      {
        path: '/links',
        title: 'Links',
        content: pageLoader(() => import("./components/links.md"))
      },
      {
        path: '/radio-buttons',
        title: 'Radio buttons',
        content: pageLoader(() => import("./components/radio-buttons.md"))
      },
      {
        path: '/tables',
        title: 'Tables',
        content: pageLoader(() => import("./components/tables.md"))
      },
      {
        path: '/text-inputs',
        title: 'Text inputs',
        content: pageLoader(() => import("./components/text-inputs.md"))
      },
      {
        path: '/text-areas',
        title: 'Text areas',
        content: pageLoader(() => import("./components/text-areas.md"))
      },
      {
        path: '/toggles',
        title: 'Toggles',
        content: pageLoader(() => import("./components/toggles.md"))
      },
      // Other subpages of 'Basics'
    ]
  },
  {
    title: 'Design Patterns',
    basePath: '/desing-patterns',
    pages: [
      {
        path: '/messages',
        title: 'Messages',
        content: pageLoader(() => import("./design-patterns/messages.md"))
      },
      // Other subpages of 'Basics'
    ]
  },
  {
    title: 'Design Handbook',
    basePath: '/desing-handbook',
    pages: [
      {
        path: '/tools',
        title: '🔳 Tool - Abstract',
        content: pageLoader(() => import("./design-handbook/tool-abstract.md"))
      },
      {
        path: '/tools',
        title: '💎 Tool - Sketch',
        content: pageLoader(() => import("./design-handbook/tool-sketch.md"))
      },
      // Other subpages of 'Basics'
    ]
  },
  {
    title: 'Assets',
    pages: [
      {
        path: '/assets',
        title: 'Download',
        content: pageLoader(() => import("./assets.md"))
      },
    ]
  },

];



// ReactDOM.render(
//  <Catalog title="Catalog" pages={pages} />,
//  document.getElementById("catalog")
// );


ReactDOM.render(
  <Catalog
    title="X-Roots DSL Docs"
    pages={pages}
    styles={style}
    logoSrc={"https://dgpniqc313w3y.cloudfront.net/1.0.0-beta.92/img/public/Logo.svg"}
    sidebarColor={ 
      {
        Text: "#4E26A6"
      } 
    }
    theme={
      {
        brandColor: "#636672",
        textColor: "#636672",
        background: "#FAFAFA",
        linkColor: "#4E26A6",
        pageHeadingBackground: "#4E26A6",
        pageHeadingTextColor: "#FFFFFF",
        navBarTextColor: "#636672",
        navBarlinkColor: "#4E26A6",
        sidebarColorText: "#A692D2",
        sidebarColorTextActive: "#4E26A6"
      }
    }
    />,
  document.getElementById("catalog")
);
