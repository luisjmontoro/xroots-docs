
## 🔳 **ABSTRACT**


> **[Abstract](https://www.goabstract.com/) es una aplicación que permite aplicar a Sketch un sistema de control de versiones, como Git en desarrollo, a través de una interfaz. Se puede utilizar tanto en webapp como en su propia aplicación nativa ([descargar app](https://app.goabstract.com/download)).**

***

### **1. Introducción: por qué Abstract**

El control de versiones de Abstract permite que varios diseñadores del equipo puedan, de forma colaborativa, trabajar en un mismo archivo de Sketch, evitando crear versiones duplicadas y protegiendo el siempre el trabajo. Al eliminar las versionas duplicadas de los archivos de Sketch (normalmente cada diseñador tiene una versión del archivo en su máquina local), tenemos siempre la certeza de saber cuál es el archivo final (el *master*).


Además, Abstract también establece y asienta algunos procesos y flujos de trabajo determinados.


#### **1.1 La organización en Abstract**

Abstract cuenta con espacios de trabajo en los que muestra los proyectos. Para diferenciar los proyectos entre sí, podemos aplicarles un color concreto en valor hexadecimal y un título. Como buena práctica, al título le aplicamos un prefijo en forma de etiqueta entre corchetes que determina el producto al que pertenece el proyecto. Posteriormente indicamos el título del proyecto seguido de una inicial que indica si es un repositorio (R.), una funcionalidad (F.) o un proyecto (P.). Por ejemplo:

 ```
[DSL] X-Roots / R.
```

En este caso el prefijo indica que se trata del sistema de diseño y el sufijo nos dice que es un repositorio.


#### **1.2 Organización de archivos dentro de un proyecto o repositorio**

Dentro de un proyecto o repositorio de Abstract encontraremos archivos de Sketch  diferentes según se trate de uno u otro:

- **Proyecto**:
  - Archivo o archivos que contienen el visual del producto.
  - Librerías propias o enlazadas desde un repositorio.
- **Repositorio**:
  - Librerías propias del repositorio disponibles para enlazar desde cualquier proyecto de Abstract.

***


### **2. Conceptos, procesos y nomenclatura**

#### **2.1 Master**

La rama master es la que contiene la última versión estable del producto dentro de un proyecto o repositorio (su particular y único **source of truth**). Todo lo que esté en el **master** ha de estar listo para validación.

Cualquier miembro del equipo puede abrir cualquier archivo de Sketch **directamente desde master**, pero, en este caso, Abstract **no registrará los cambios que haga**. Esta es la forma más segura de enseñar el visual o realizar experimentos sobre el diseño sin destruirlo.

#### **2.2 Ramas (*branches*)**

Las ramas son versiones que se crean del proyecto, a partir del **master**, en las que cada diseñador podrá realizar ajustes, modificaciones o desarrollar nuevas funcionalidades sin que estas alteren la estabilidad del mismo. Son espacios independientes donde trabajar e ir guardando los cambios sin modificar la última versión estable del producto. Cada miembro del equipo implicado en un mmismo proyecto trabaja en una rama diferente. Y cada rama, a su vez, puede diverger en diferentes ramas, de modo que distintas personas pueden trabajar en la misma funcionalidad sin necesidad de pisarse sus ramas entre sí.

El nombre de cada rama deberá tener la siguiente anatomía:

 ```
 Código de la tarea en Jira - Descripción - Nombre de usuario
```

Por ejemplo: 

 ```
  DSL03 - Formularios - Luis
```

##### **2.2.1 Estados de las ramas (Branch status)**

En Abstract, a cada rama se le puede asignar un estado (**branch status**) mediante un botón que se encuentra junto al título de la rama. Hay tres estados posibles: **work in progress**, **open for feedback** y **review**.

Cómo usamos los estados de las ramas:

- **Work in progress**: lo utilizamos para ramas que, por las características de su trabajo, van a tener una duración de más de un día.
- **Open for feedback**: marcamos este estado cuando necesitemos validar el trabajo internamente.
- **Review**: este estado se marca de forma diferente al resto. Hay que solicitar una *review* de la rama para ello. Marcamos la rama como lista para **review** cuando el diseño está validado interna y externamente. La revisión nos sirve para asegurarnos de que los cambios de la rama no van a perjudicar la estabilidad de *master* (revisamos artboards, enlaces correctos a librerías, etc.).

#### **2.3 Commits**

Para que Abstract registre y guarde los cambios en cada rama no basta con guardar desde el fichero de Sketch, sino que tenemos que **hacer un commit**. Cada commit hace una subida controlada de datos a una rama concreta. No se puede commitear sobre *master*.
 
Es importante hacer commits a menudo, con actualizaciones pequeñas, ya que es mucho más manejable a la hora de resolver posibles conflictos, revertir commits o incluso sincronizar las modificaciones con Abstract.

La anatomía de un commit deberá ser:

```
Título - descripción muy concisa

Descripción:
✅ Añadido - Nombre de lo añadido.
🛠 Modificado - Nombre de lo modificado.
❌ Eliminado -  Nombre de lo eliminado.
```

Por ejemplo: 

```
Text inputs - nuevos tamaños

✅ Añadido - Tamaños s, m, l y xl
✅ Añadido - Excepción de tamaño para flujo de compra.
🛠 Modificado - Alto en tamaño S.
❌ Eliminado -  Artboards de borradores.
```

```hint
⚠️ Es de vital importancia respetar y seguir esta nomenclatura ya que ofrece una visión mucho más concreta y precisa en el histórico de commits en caso de que hubiera que restaurar una rama (o incluso master) a un estado anterior.
```

#### **2.4 Request review y Merge**

Un **merge** es la fusión de nuestra rama personal con la rama master. Es la forma de llevarnos todos los ajustes o aportaciones de nuestra rama a la última versión estable del proyecto (*master*).

Para que un merge se lleve a cabo, previamente se ha de pedir una **review request** mediante la que la rama ha de ser validada antes de que los cambios suban a *master*.

```hint
⛔️ MUY IMPORTANTE: para garantizar la estabilidad del proyecto, ninguna rama ha de ser mergeada con master sin haber sido validada y aprobada.
```

***

### **3 Procesos en Abstract**

#### **3.1 Nuevo proyecto**

1. Creación  en Abstract, por parte de un administrador,  de un **nuevo proyecto** con el prefijo y sufijo correspondiente.
2. Creación del **archivo** de Sketch principal en la rama Master.
3. Enlace de las **librerías** del repositorio o repositorios necesarios.
4. Creación de una **rama** aplicando la nomenclatura correspondiente (*Código de la tarea - Descripción - Nombre de usuario*).
5. Generación de los visuales necesarios y gestión de los mismos mediante **commits**.
6. Una vez el trabajo esté realizado, se creará la petición de una **review request**.
7. Una vez validada la rama por un administrador o el PM, se realizará el **merge** de la rama a *Master*.

#### **3.2 Nueva contribución al repositorio del DSL**

1. Creación de una **nueva rama** desde Master, aplicando la nomenclatura correspondiente (*Código de la tarea - Descripción - Nombre de usuario*)
2. Creación del archivo de Sketch directamente en Abstract como **librería**. La nomenclatura variará en función de si se trata de un primitive o un componente:
  - **Primitive**: Letra del alfabeto correspondiente - prefijo entre corchetes - nombre del primitive ⤵️ 
  ```
  a - [pr] color.sketch
  ```
  -  **Componente**: Prefijo entre corchetes - Número de tres cifras - nombre del componente ⤵️
  ```
  [Comp] - 001 - buttons.sketch
  ```
3. Creación de **commits** siguiendo la nomenclatura correspondiente.
4. Una vez el trabajo esté realizado, se creará la petición de una **review request**.
5. Una vez validada la rama por un administrador o el PM, se realizará el **merge** de la rama a *Master*.




