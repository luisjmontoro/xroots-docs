## Sketch
---

### Libraries

#### Color

```download
title: Primitive Color.sketch
subtitle: 115 KB
url: https://orangexes.sharepoint.com/:u:/r/sites/General/Documentos%20compartidos/UX/4.%20DSL/Repositorio/01.%20Primitives/a%20-%20primitive%20color%20(master).sketch?csf=1&e=rLTYhZ
```
#### Sketch flat color palette

```download
title: xroots_flat_color.sketchpalette
subtitle: 115 KB
url: https://orangexes.sharepoint.com/:u:/r/sites/General/Documentos%20compartidos/UX/4.%20DSL/Repositorio/z.%20utility%20-%20xroots_flat_color_palette.sketchpalette?csf=1&e=0BIquZ
```
#### Tipografía

```download
title: Primitive Typography.sketch
subtitle: 332 KB
url: https://orangexes.sharepoint.com/:u:/r/sites/General/Documentos%20compartidos/UX/4.%20DSL/Repositorio/01.%20Primitives/b%20-%20primitive%20typo%20(master).sketch?csf=1&e=0eDHSr
```

#### Grid

```download
title: Utilities - Grid.sketch
subtitle: 18 KB
url: https://orangexes.sharepoint.com/:u:/r/sites/General/Documentos%20compartidos/UX/4.%20DSL/Repositorio/z.%20utility%20-%20grid.sketch?csf=1&e=BWHXyF
```

## Front
---
### Design tokens repository

[Gitlab](https://gitlab.com/OrangeX/frontend/resources/design-tokens)



