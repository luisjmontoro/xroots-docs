## Definición
> **Los mensajes dan información puntual al usuario en la web sobre la que es necesario hacer énfasis. Engloba tanto la comprensión de un problema y su solución, como la comunicación de avisos de diferente relevancia. Aunque son diferentes en cuanto a contenido, contexto y estado, comparten la misma premisa: cubrir necesidades tanto del negocio como del usuario para hacer más amigable un servicio y prevenir errores.**

## **1. Tipos de aviso**

```table
span: 6
columns:
  - Mensaje
  - Uso

rows:
  - Mensaje: '**info**'
    Uso: 'Notificación que aporta al usuario información neutral que suele corresponder con feedback sobre el estado del sistema. **No bloqueante.**'
  - Mensaje: '**success**'
    Uso: 'Mensaje de feedback positivo respecto a alguna interacción del usuario. **No bloqueante.**'
  - Mensaje: '**warning**'
    Uso: 'Aviso sobre algunna interacción requerida por el usuario o advertencia sobre alguna interacción próxima. **No Bloqueante.**'
  - Mensaje: '**error**'
    Uso: 'Mensaje que comunica un fallo por parte del sistema o por parte del usuario. **Bloqueante.**' 
```
```image
    span: 6
    src: "https://gitlab.com/luisjmontoro/xroots-docs/raw/799432a48f9d5f59e02b4c8085a9c9e41613b839/catalog/static/img/patterns-messages-iconos-leyenda.png"
    plain: true
    description: "_Iconos de avisos_"
```

## **2. Patrones**

### 2.1. Mensajes corporativos

Los mensajes **corporativos** son avisos que lanzamos sin necesidad de interacción previa por parte del usuario. Pueden ser avisos informativos sobre nuevas funcionalidades, avisos sobre cambios en los términos y condiciones de uso de la web, etc.

El **tono** ha de ser informativo, explicando brevemente el motivo de la comunicación e invitando al usuario, en caso necesario, a ampliar información a través de un **link**.

Componentes y patrones de uso:

- **[Floating Banner](#3-patrones)**
  - info
  - warning
  - success
- **[Banner](#3-patrones)**
  - info
  - warning
  - success




### 2.2. Feedback de interacción

Los avisos de **interacción** son aquellos que aparecen como consecuencia de una interacción previa del usuario. Normalmente serán del tipo **warning** (ej. aviso sobre la cobertura del servicio de fibra), **error** (ej. error de formulario) o **success** (ej. modal de confirmación).

El **tono** ha de ser directo y conciso. En el caso de los errores, el mensaje debe indicar al usuario el motivo del error y las instrucciones para solucionarlo.

Componentes y patrones de uso:

- **[Floating Banner](#3-patrones)**
  - success
  - warning
  - error
- **[Banner](#3-patrones)**
  - error
- **[Input](#3-patrones)**
  - error
  - success
- **[Modal](#3-patrones)**
  - success
  - warning
  - error


```image
    span: 6
    src: "https://gitlab.com/luisjmontoro/xroots-docs/raw/e627c276c611d8a7663fa482f8ad7fbf8d2ed34e/catalog/static/img/pattern-message-floating-success.png"
    plain: true
    description: "Ejemplo: floating banner / success"
```
```image
    span: 6
    src: "https://gitlab.com/luisjmontoro/xroots-docs/raw/e627c276c611d8a7663fa482f8ad7fbf8d2ed34e/catalog/static/img/pattern-message-banner-flow.png"
    plain: true
    description: "Ejemplo: banner / warning"
```
```image
    span: 6
    src: "https://gitlab.com/luisjmontoro/xroots-docs/raw/e627c276c611d8a7663fa482f8ad7fbf8d2ed34e/catalog/static/img/pattern-message-input-error.png"
    plain: true
    description: "Ejemplo: input / error"
```


### 2.3. Avisos de sistema

Los avisos de **sistema** indican al usuario un estado concreto del sistema. Normalmente son errores o avisos que impiden al usuario realizar una tarea concreta en un determinado momento. Normalmente son ajenos a la interacción del usuario, salvo en los del tipo **404**.

El **tono** del aviso ha de ser informativo y el mensaje ha de facilitar lo máximo posible al usuario que pueda completar la tarea que necesita. En el caso de ser un error del sistema, debe proveer información acerca de cuándo se podrá usar el producto con normalidad.

Componentes y patrones de uso:

- **[Banner](#3-patrones)**
  - error
  - warning
- **[Página completa](#3-patrones)**

```image
    span: 6
    src: "https://gitlab.com/luisjmontoro/xroots-docs/raw/e627c276c611d8a7663fa482f8ad7fbf8d2ed34e/catalog/static/img/pattern-message-banner-system.png"
    plain: true
    description: "Ejemplo: banner / error"
```

***
## **3. Componentes**

```table
span: 6
columns:
  - Tipo
  - Patrón de uso

rows:
  - Tipo: '**Banner**'
    Patrón de uso: 'Banner localizado en la parte superior de la página o de la vista del contenido. Ocupa el 100% de la vista del contenido.'
  - Tipo: '**Floating banner**'
    Patrón de uso: 'Banner con layer **floating** con posición absoluta sobre el contenido en la esquina superior derecha de la página.'
  - Tipo: '**Modal**'
    Patrón de uso: 'Ventana modal con layer **floating** sobre un lightbox.'
  - Tipo: '**Input**'
    Patrón de uso: 'Mensaje localizado en la parte inferior de un campo de texto.'
  - Tipo: '**Página de error**'
    Patrón de uso: 'Página completa que impide realizar cualquier acción dentro de la misma.'    
``` 
 



## **4. Propiedades**

### 4.1 Color

```color
span: 1
value: '#467EF3'
name: 'info-base'
```
```color
span: 1
value: '#32C832'
name: 'success-base'
```
```color
span: 1
value: '#FFCC00'
name: 'warning-base'
```
```color
span: 1
value: '#DB4B24'
name: 'error-base'
```

```table
span: 6
columns:
  - Tipo
  - Border and icon
  - Font
rows:
  - Tipo: '**Info**'
    Border and icon: '{color.root.ui.support.info.base.value}'
    Font: '{color.font.indigo.base.value}'
  - Tipo: '**Success**'
    Border and icon: '{color.root.ui.support.success.base.value}'
    Font: '{color.font.indigo.base.value}'
  - Tipo: '**Warning**'
    Border and icon: '{color.root.ui.support.warning.base.value}'
    Font: '{color.font.indigo.base.value}'
  - Tipo: '**Error**'
    Border and icon: '{color.root.ui.support.error.base.value}'
    Font: '{color.font.indigo.base.value}'

```
